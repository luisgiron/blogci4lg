-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: db
-- Tiempo de generación: 30-05-2020 a las 19:17:01
-- Versión del servidor: 8.0.19
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ci4`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int NOT NULL,
  `nombre` text NOT NULL,
  `direccion` text NOT NULL,
  `creado_en` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizado_en` date DEFAULT NULL,
  `eliminado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `nombre`, `direccion`, `creado_en`, `actualizado_en`, `eliminado`) VALUES
(1, 'Yirone2', 'Avda Sucre2', '2020-05-19 20:04:26', '2020-05-29', 0),
(2, 'Yoya', 'Los Simbolos', '2020-05-19 20:04:26', NULL, 0),
(3, 'Susan Corleonne', 'El silencio', '2020-05-21 18:23:43', NULL, 0),
(4, 'Luis 1', 'Torre I', '2020-05-27 19:13:04', NULL, 0),
(5, 'Luis 2', 'Torre II', '2020-05-27 19:14:34', NULL, 0),
(6, 'Luis 3', 'Torre III', '2020-05-27 19:18:01', NULL, 0),
(7, 'Luis 4', 'Torre IV', '2020-05-27 19:19:24', NULL, 0),
(8, 'Luis 5', 'Torre V', '2020-05-27 19:34:17', NULL, 0),
(9, 'Luis 6', 'Torre Vi', '2020-05-27 19:42:47', NULL, 0),
(10, 'Luis 7', 'Torre VII', '2020-05-27 19:53:11', NULL, 0),
(11, 'Luis 8', 'Torre VIII', '2020-05-27 19:59:44', NULL, 0),
(12, 'Luis 9', 'Torre IX', '2020-05-27 20:02:21', NULL, 0),
(13, 'Luis 10', 'Torre X', '2020-05-27 20:03:41', NULL, 0),
(14, 'lUIS 11', 'Torre XI', '2020-05-27 20:04:43', NULL, 0),
(15, 'Luis 12', 'Torre XII', '2020-05-28 00:31:23', NULL, 0),
(16, 'Luis 13', 'Torre XIII', '2020-05-28 00:32:01', NULL, 0),
(17, 'Luis 14', 'Torre XIV', '2020-05-28 00:33:19', NULL, 0),
(18, 'Luis 15', 'Torre XV', '2020-05-28 00:34:05', NULL, 0),
(19, 'Luis 16', 'Torre', '2020-05-28 00:34:33', '2020-05-29', 0),
(20, 'Luis 177', 'Torre XVII7', '2020-05-29 14:58:59', '2020-05-29', 1),
(21, 'Luis 187', 'Torre XVIII7', '2020-05-29 18:21:16', '2020-05-29', 1),
(22, 'Luis 19', 'Torre XIX', '2020-05-29 19:14:39', '2020-05-29', 1),
(23, 'Luis 20', 'Torre XX', '2020-05-29 19:16:50', NULL, 0),
(24, 'Luis 21', 'Torre XXI', '2020-05-29 19:17:55', '2020-05-29', 1),
(25, 'Luis 22', 'Torre XXII', '2020-05-29 19:21:00', '2020-05-29', 1),
(26, 'Luis 23', 'Torre XXIII', '2020-05-29 19:22:40', '2020-05-29', 1),
(27, 'Luis 24', 'Torre XXIV', '2020-05-29 19:25:15', '2020-05-29', 1),
(28, 'Luis 255', 'Torre XXVV', '2020-05-29 19:27:17', '2020-05-29', 1),
(29, 'Luis 26', 'Torre XXVI', '2020-05-29 19:28:51', '2020-05-29', 1),
(30, 'Luis 27', 'Torre XXVII', '2020-05-29 19:42:54', '2020-05-29', 1),
(31, 'Luis 27', 'Torre XXVII', '2020-05-29 19:43:07', '2020-05-29', 1),
(32, 'Luis 28', 'Torre XXVIII', '2020-05-29 19:44:20', '2020-05-29', 1),
(33, 'Luis 299', 'Torre 299', '2020-05-29 21:48:54', '2020-05-29', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
