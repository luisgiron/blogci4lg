-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: db
-- Tiempo de generación: 18-10-2020 a las 19:06:37
-- Versión del servidor: 8.0.19
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ci4`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos_subidos`
--

CREATE TABLE `archivos_subidos` (
  `id` int NOT NULL COMMENT 'Primary Key',
  `nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Name',
  `tipo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'file type',
  `ruta` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `usuario` text NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='demo table';

--
-- Volcado de datos para la tabla `archivos_subidos`
--

INSERT INTO `archivos_subidos` (`id`, `nombre`, `tipo`, `ruta`, `usuario`, `fecha`) VALUES
(1, '084019523987_pago_10_dolares_sabado_31082019.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(2, 'ANTECEDENTES_DE_SERVICIO_LG.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(3, 'ANVERSO_CONSTANCIA_DE_JUBILACION.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(4, 'CONSEJO NACIONAL ELECTORAL - Datos del Elector.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(5, 'Constancia de Residencia.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(6, 'cuenta_bancaria.odt', 'odt', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(7, 'ANTECEDENTES_DE_SERVICIO_LG_1.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(8, 'ANVERSO_CONSTANCIA_DE_JUBILACION_1.pdf', 'pdf', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(9, 'cuenta_bancaria_12_02_2020.odt', 'odt', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08'),
(10, 'cuenta_bancaria_montaÃ±a.odt', 'odt', '/var/www/html/ci4/uploads/documentos/multiple/', 'Luis Giron', '2020-09-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_users`
--

CREATE TABLE `blog_users` (
  `id` int NOT NULL,
  `correo` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `creacion` datetime NOT NULL,
  `actualizacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `blog_users`
--

INSERT INTO `blog_users` (`id`, `correo`, `clave`, `creacion`, `actualizacion`) VALUES
(1, 'luis.giron@micorreo.com', '123456', '2020-10-14 00:00:00', '2020-10-14 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cedulas`
--

CREATE TABLE `cedulas` (
  `id` int NOT NULL,
  `nacionalidad` text NOT NULL,
  `cedula` int NOT NULL,
  `primerapellido` text NOT NULL,
  `segundoapellido` text NOT NULL,
  `primernombre` text NOT NULL,
  `segundonombre` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `cedulas`
--

INSERT INTO `cedulas` (`id`, `nacionalidad`, `cedula`, `primerapellido`, `segundoapellido`, `primernombre`, `segundonombre`) VALUES
(1, 'V', 6730292, 'GIRON', 'AGUIAR', 'LUIS', 'BELTRAN'),
(2, 'V', 6316113, 'GIOIA', 'VITALE', 'MARIA', 'ASSUNTA'),
(3, 'V', 30123123, 'LARA', 'LARA2', 'MARIABONITA', 'CHAMA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id` int NOT NULL COMMENT 'Primary Key',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `email` varchar(255) NOT NULL COMMENT 'Email Address',
  `message` varchar(250) NOT NULL COMMENT 'Message',
  `created_at` varchar(20) NOT NULL COMMENT 'Created date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='demo table';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int NOT NULL,
  `nombre` text NOT NULL,
  `sexo` text NOT NULL,
  `extranjero` int NOT NULL,
  `direccion` text NOT NULL,
  `id_estado` int NOT NULL DEFAULT '2',
  `id_municipio` int NOT NULL DEFAULT '0',
  `id_parroquia` int NOT NULL,
  `creado_en` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizado_en` date DEFAULT NULL,
  `eliminado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `nombre`, `sexo`, `extranjero`, `direccion`, `id_estado`, `id_municipio`, `id_parroquia`, `creado_en`, `actualizado_en`, `eliminado`) VALUES
(1, 'Mi sueÃ±o mÃ¡s dorado', '', 0, 'Algo ardoroso', 4, 12, 0, '2020-07-01 01:05:36', '2020-07-01', 0),
(2, 'Me puse ocioso', '', 0, 'Silvio', 1, 1, 0, '2020-07-01 01:06:35', '2020-07-01', 1),
(3, 'Si era soÃ±ando', '', 0, 'Huele a atÃºn', 5, 13, 0, '2020-07-01 01:07:05', '2020-07-01', 1),
(4, 'A donde irÃ¡n', '', 0, 'Como prisioneras', 4, 11, 0, '2020-07-01 01:09:52', '2020-07-01', 1),
(5, 'Mis viejos zapatos', '', 0, 'Tantas hojas de un Ã¡rbol', 3, 10, 0, '2020-07-01 01:10:21', '2020-07-07', 1),
(6, 'Te doy una canciÃ³n', '', 0, 'Silvio', 2, 4, 0, '2020-07-01 01:42:48', NULL, 0),
(7, 'Yolanda', '', 0, 'miÃ©rcoles', 3, 9, 0, '2020-07-01 14:35:58', NULL, 0),
(8, 'Registro para Caracas', '', 0, 'Jueves', 1, 1, 0, '2020-07-02 19:03:18', NULL, 0),
(9, 'Registro para Yaracuy', '', 0, 'Jueves', 6, 14, 0, '2020-07-02 20:31:46', NULL, 0),
(10, 'Registro para Lara', '', 0, 'Jueves', 5, 16, 0, '2020-07-02 20:56:53', '2020-07-02', 0),
(11, 'Registro lunes seis', '', 0, 'Guacara', 4, 11, 0, '2020-07-06 22:34:27', NULL, 0),
(12, 'Registro del dia siete', '', 0, 'Martes', 4, 12, 25, '2020-07-07 19:54:03', NULL, 0),
(13, 'Segundo Registro del siete', '', 0, 'Martes 7', 4, 11, 23, '2020-07-07 19:56:08', '2020-07-07', 0),
(14, 'Registro para probar el reset', 'M', 0, 'martes 7', 3, 10, 20, '2020-07-07 20:00:50', NULL, 0),
(15, 'Registro uno del miercoles', 'M', 1, 'miercoles 8', 4, 12, 25, '2020-07-08 22:41:20', '2020-07-09', 0),
(16, 'Primer Registro del jueves', 'M', 1, 'Jueves', 4, 12, 25, '2020-07-09 20:23:25', '2020-07-09', 0),
(17, 'Nicolas Tesla', 'M', 1, 'Nueva York', 4, 12, 25, '2020-07-27 22:16:13', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int NOT NULL,
  `estado` text NOT NULL,
  `eliminado` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `estado`, `eliminado`) VALUES
(1, 'Dtto Capital', 0),
(2, 'Miranda', 0),
(3, 'Aragua', 0),
(4, 'Carabobo', 0),
(5, 'Lara', 0),
(6, 'Yaracuy', 0),
(7, 'Amazonas', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluaciones`
--

CREATE TABLE `evaluaciones` (
  `idEvaluacion` int NOT NULL,
  `idEmpleado` int NOT NULL,
  `primertrimestre` int NOT NULL,
  `segundotrimestre` int NOT NULL,
  `tercertrimestre` int NOT NULL,
  `cuartotrimestre` int NOT NULL,
  `evaluacionfinal` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `evaluaciones`
--

INSERT INTO `evaluaciones` (`idEvaluacion`, `idEmpleado`, `primertrimestre`, `segundotrimestre`, `tercertrimestre`, `cuartotrimestre`, `evaluacionfinal`) VALUES
(1, 17, 10, 20, 15, 13, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `id` int NOT NULL,
  `municipio` text NOT NULL,
  `id_estado` int NOT NULL,
  `eliminado` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id`, `municipio`, `id_estado`, `eliminado`) VALUES
(1, 'Libertador', 1, 0),
(2, 'Chacao', 2, 0),
(3, 'Acevedo', 2, 0),
(4, 'Andres Bello', 2, 0),
(5, 'Baruta', 2, 0),
(6, 'Carrizal', 2, 0),
(9, 'Iragorry', 3, 0),
(10, 'Girardot', 3, 0),
(11, 'Guacara', 4, 0),
(12, 'San Joaquin', 4, 0),
(13, 'Palavecino', 5, 0),
(14, 'San Felipe', 6, 0),
(15, 'Cocorote', 6, 0),
(16, 'Barquisimeto', 5, 0),
(17, 'Bobure', 5, 0),
(18, 'Atabapo', 7, 0),
(19, 'Alto Orinoco', 7, 0),
(20, 'Atures', 7, 0),
(21, 'Autana', 7, 0),
(22, 'Rio Negro', 7, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parroquia`
--

CREATE TABLE `parroquia` (
  `id` int NOT NULL,
  `parroquia` text NOT NULL,
  `id_municipio` int NOT NULL,
  `eliminado` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `parroquia`
--

INSERT INTO `parroquia` (`id`, `parroquia`, `id_municipio`, `eliminado`) VALUES
(1, 'La Pastora', 1, 0),
(2, 'Altagracia', 1, 0),
(3, 'Antimano', 1, 0),
(4, '23 de Enero', 1, 0),
(7, 'La Candelaria', 1, 0),
(8, 'Caricuao', 1, 0),
(9, 'Coche', 1, 0),
(10, 'El Junquito', 1, 0),
(11, 'El Valle', 1, 0),
(12, 'El Recreo', 1, 0),
(13, 'La Vega', 1, 0),
(14, 'Huachamacare', 19, 0),
(15, 'La Esmeralda', 19, 0),
(16, 'San José de Chacao', 2, 0),
(17, 'El Limon', 9, 0),
(18, 'Cania de Azucar', 9, 0),
(19, 'Choroni', 10, 0),
(20, 'Las Delicias', 10, 0),
(21, 'Ciudad Alianza', 11, 0),
(22, 'Yagua', 11, 0),
(23, 'Guacara Parr', 11, 0),
(24, 'Parroquia SJ 1', 12, 0),
(25, 'Parroquia SJ 2', 12, 0),
(26, 'Parr Palav 1', 13, 0),
(27, 'Parr Palav 2', 13, 0),
(28, 'Parr 1 San Felipe', 14, 0),
(29, 'Parr 2 San Felipe', 14, 0),
(30, 'Parr 1 Cocorote', 15, 0),
(31, 'Parr 2 Cocorote', 15, 0),
(32, 'Parr A Acevedo', 3, 0),
(33, 'Parr B Acevedo', 3, 0),
(34, 'Parr de Andres Bello', 4, 0),
(35, 'Parr Baruta 1', 5, 0),
(36, 'Parr Baruta 2', 5, 0),
(37, 'Parr Carrizal 1', 6, 0),
(38, 'Parr Carrizal 2', 6, 0),
(39, 'Parroq 1 Barquisimeto', 16, 0),
(40, 'Parroq 2 Barquisimeto', 16, 0),
(41, 'Parr Bobure 1', 17, 0),
(42, 'Parr Bobure 2', 17, 0),
(43, 'Parr atabapo 1', 18, 0),
(44, 'Parr atabapo 2', 18, 0),
(45, 'Parr Atures 1', 20, 0),
(46, 'Parr atures 2', 20, 0),
(47, 'Parr autana 1', 21, 0),
(48, 'Parr Autana 2', 21, 0),
(49, 'Parr Rio Negro 1', 22, 0),
(50, 'Parr Rio Negro 2', 22, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `post_id` int NOT NULL,
  `post_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `post_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `post_author` int NOT NULL,
  `post_created_at` datetime NOT NULL,
  `post_updated_at` datetime NOT NULL,
  `post_deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`post_id`, `post_title`, `post_content`, `post_author`, `post_created_at`, `post_updated_at`, `post_deleted_at`) VALUES
(1, 'Primer TÃ­tulo', 'Primer Contenido', 1, '0000-00-00 00:00:00', '2020-10-14 06:34:04', '0000-00-00 00:00:00'),
(2, 'Segundo TÃ­tulo', 'Segundo Contenido', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Tercer TÃ­tulo', 'Tercer Contenido', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Cuarto TÃ­tulo', 'Cuarto Contenido', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Quinto TÃ­tulo', 'Quinto Contenido', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Sexto TÃ­tulo', 'Sexto Contenido', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Septimo TÃ­tulo', 'Septimo contenido', 1, '2020-10-13 19:43:35', '2020-10-13 19:43:35', '0000-00-00 00:00:00'),
(8, 'Octavo TÃ­tulo', 'Octavo Contenido', 1, '2020-10-13 20:16:38', '2020-10-13 20:25:03', '0000-00-00 00:00:00'),
(9, 'Noveno TÃ­tulo', 'Noveno Contenido', 1, '2020-10-13 20:17:09', '2020-10-13 20:24:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sys_usuario`
--

CREATE TABLE `sys_usuario` (
  `id` int NOT NULL,
  `nombre_completo` text NOT NULL,
  `correo` text NOT NULL,
  `clave` int NOT NULL,
  `id_rol` int NOT NULL,
  `activo` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `creado_en` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizado_en` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `eliminado` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `correo`, `creado_en`, `actualizado_en`, `eliminado`) VALUES
(76, 'Lunes dosx Agregado', 'lunes@algo.com', '2020-05-11 22:30:08', '2020-05-11 22:30:08', 2020),
(77, 'Lunes dosx Agregado', 'lunes@algo.com', '2020-05-11 22:30:31', '2020-05-11 22:30:31', 2020),
(78, 'tacata tacata Agregado', 'lunes@algo.com', '2020-05-11 22:31:37', '2020-05-11 22:31:37', NULL),
(79, 'paul mccartey Agregado', 'lunes@algo.com', '2020-05-11 22:33:47', '2020-05-11 22:33:47', NULL),
(80, 'Shon Lennonx Agregado', 'lunes@algo.com', '2020-05-11 22:36:57', '2020-05-11 22:36:57', NULL),
(81, 'Jeorge Jarrison Agregado', 'lunes@algo.com', '2020-05-11 22:39:01', '2020-05-11 22:39:01', NULL),
(82, 'Jeorge Jarrison Agregado Agregado', 'lunes@algo.com', '2020-05-11 22:40:50', '2020-05-11 22:40:50', NULL),
(83, 'Ricardo la estrella Agregado', 'lunes@algo.com', '2020-05-11 22:41:07', '2020-05-11 22:41:07', NULL),
(84, 'Ricardo la estrella Agregado Agregado Agregado', 'lunes@algo.com', '2020-05-11 22:41:30', '2020-05-11 22:41:30', NULL),
(85, 'Ringo Starr Agregado', 'martestambien@algo.com', '2020-05-11 22:43:23', '2020-05-11 22:43:23', NULL),
(86, 'Brayan Epstain Agregado', 'martes@algo.com', '2020-05-11 22:43:40', '2020-05-11 22:43:40', NULL),
(87, 'Se grego por detras Agregado', 'jueves@algo.com', '2020-05-14 19:01:20', '2020-05-14 19:01:20', NULL),
(88, 'Este se Agrego el jueves tambien Agregado', 'jueves@algo.com', '2020-05-14 19:30:25', '2020-05-14 19:30:25', NULL),
(89, 'Que manden agua Agregado', 'jueves@algo.com', '2020-05-14 20:40:12', '2020-05-14 20:40:12', NULL),
(90, 'Paper Back Writer Song Agregado', 'lunes18@algo.com', '2020-05-18 20:02:44', '2020-05-18 20:02:44', NULL),
(91, 'martes veintiseis mayo Agregado', 'martestambien@algo.com', '2020-05-26 20:34:58', '2020-05-26 20:34:58', NULL),
(92, 'Otro del martes veintiseis Agregado', 'algo@dd.com', '2020-05-26 20:40:55', '2020-05-26 20:40:55', 2020),
(93, 'Gasolina para la Prod Agregado', 'martes2605@algo.com', '2020-05-26 20:42:51', '2020-05-26 20:42:51', NULL),
(94, 'sfdas asf s a sda Agregado', 'aa@dd.com', '2020-05-26 20:49:30', '2020-05-26 20:49:30', NULL),
(95, 'Nicolas Maduro Agregado', 'nicolas@algo.com', '2020-05-26 20:50:15', '2020-05-26 20:50:15', NULL),
(96, 'Este era el nueve dos Agregado', 'aa@dd.com', '2020-05-26 20:50:39', '2020-05-26 20:50:39', NULL),
(97, 'Agregado martes  dos del seis Agregado', 'dosdelseis@algo.com', '2020-06-02 18:56:48', '2020-06-02 18:56:48', NULL),
(98, 'Luis Giron', 'lb2giron@gmail.com', '2020-06-14 16:12:27', '2020-06-14 16:12:27', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos_subidos`
--
ALTER TABLE `archivos_subidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blog_users`
--
ALTER TABLE `blog_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cedulas`
--
ALTER TABLE `cedulas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  ADD PRIMARY KEY (`idEvaluacion`),
  ADD KEY `fk_id_empleado` (`idEmpleado`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `parroquia`
--
ALTER TABLE `parroquia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indices de la tabla `sys_usuario`
--
ALTER TABLE `sys_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivos_subidos`
--
ALTER TABLE `archivos_subidos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `blog_users`
--
ALTER TABLE `blog_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cedulas`
--
ALTER TABLE `cedulas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  MODIFY `idEvaluacion` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `parroquia`
--
ALTER TABLE `parroquia`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `sys_usuario`
--
ALTER TABLE `sys_usuario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  ADD CONSTRAINT `fk_empleado` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
