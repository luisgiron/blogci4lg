-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: db
-- Tiempo de generación: 30-05-2020 a las 19:19:16
-- Versión del servidor: 8.0.19
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `continente`
--

CREATE TABLE `continente` (
  `id` int NOT NULL,
  `id_continente` int DEFAULT NULL,
  `continente` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `continente`
--

INSERT INTO `continente` (`id`, `id_continente`, `continente`) VALUES
(1, 1, 'América'),
(2, 2, 'Asia'),
(3, 3, 'Europa'),
(4, 4, 'Africa'),
(5, 5, 'Oceanía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int NOT NULL,
  `id_continente` int DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `id_continente`, `pais`) VALUES
(1, 1, 'Venezuela'),
(2, 1, 'Mexico'),
(3, 1, 'Chile'),
(4, 1, 'Bolivia'),
(5, 1, 'Peru'),
(6, 2, 'Japon'),
(7, 2, 'Corea'),
(8, 2, 'Indonesia'),
(9, 2, 'Filipinas'),
(10, 2, 'Singapur'),
(11, 3, 'Italia'),
(12, 3, 'España'),
(13, 3, 'Francia'),
(14, 3, 'Inglaterra'),
(15, 3, 'Holanda'),
(16, 4, 'Argelia'),
(17, 4, 'Marruecos'),
(18, 4, 'Mozambique'),
(19, 4, 'Ruanda'),
(20, 4, 'Sierra Leona'),
(21, 5, 'Australia'),
(22, 5, 'Islas Marshall'),
(23, 5, 'Isalas Fidji');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_mundo`
--

CREATE TABLE `t_mundo` (
  `id` int NOT NULL,
  `id_continente` int DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `t_mundo`
--

INSERT INTO `t_mundo` (`id`, `id_continente`, `pais`) VALUES
(1, 1, 'Mexico'),
(2, 1, 'Venezuela'),
(3, 1, 'Chile'),
(4, 1, 'Bolivia'),
(5, 1, 'Peru'),
(6, 2, 'Japon'),
(7, 2, 'Corea'),
(8, 2, 'Indonesia'),
(9, 2, 'Filipinas'),
(10, 2, 'Singapur'),
(11, 3, 'Italia'),
(12, 3, 'España'),
(13, 3, 'Francia'),
(14, 3, 'Inglaterra'),
(15, 3, 'Holanda'),
(16, 4, 'Argelia'),
(17, 4, 'Marruecos'),
(18, 4, 'Mozambique'),
(19, 4, 'Ruanda'),
(20, 4, 'Sierra Leona');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `continente`
--
ALTER TABLE `continente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_mundo`
--
ALTER TABLE `t_mundo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `continente`
--
ALTER TABLE `continente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `t_mundo`
--
ALTER TABLE `t_mundo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
