$(function()
{
     //alert('base_url='+base_url);
     listarPosts();
     /* Esto es para Llenar el DataTable con un ajax */
     function listarPosts()
     {
	  $('#table_posts').DataTable
	  (
	  {
	       "order":[[0,"desc"]],					
	       "paging":true,
	       "info":true,
	       "filter":true,
	       "responsive": true,
	       "autoWidth": false,					
	       //"stateSave":true,
	       "ajax":
	       {
		    "url":base_url+"/posts/listar",
		    "type":"POST",
		    dataSrc:''
	       },
	       "columns":
	       [
	       //{data:'id'},
	       //{data:'titulo'},
	       //{data:'autor'},
	       //{data:'fecha_publicacion'},
	       {orderable: true,
	       render:function(data, type, row)
		    {
			 return 
			 '<a href="javascript:;" class="btn btn-info editar-item" id_a_editar='+row.id+'>Editar</a>'
			 +'<a href="javascript:;" class="btn btn-danger eliminar-item" id_a_eliminar='+row.id+' titulo_a_eliminar='+row.titulo+'>Eliminar</a>'
			 +'<a href="javascript:;" class="btn btn-primary imagen-para-el-item" id_post='+row.id+ ' post_titulo='+row.titulo+'>Imagen</a>';
		    }
	       }
	       ],
	       "language":
	       {
		    //"url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		    //"url": base_url + '/empleado/espaniol'						
		    "sProcessing":    "Procesando...",
		    "sLengthMenu":    "Mostrar _MENU_ registros",
		    "sZeroRecords":   "No se encontraron resultados",
		    "sEmptyTable":    "Ningún dato disponible en esta tabla",
		    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":   "",
		    "sSearch":        "Buscar:",
		    "sUrl":           "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": 
		    {
			 "sFirst":    "Primero",
			 "sLast":    "Último",
			 "sNext":    "Siguiente",
			 "sPrevious": "Anterior"
		    },
		    "oAria":
		    {
			 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
	       },
	       "columnDefs": 
	       [
		    {
			 "targets": [ 0 ],
			 "visible": false,
			 "searchable": false
		    }
	       ],			
	  }
	  );
      }

     $('#btnAgregarPost').click(function()
     {
	 $('#ventanaModalRegistrarPosts').modal('show');
	 $('#ventanaModalRegistrarPosts').find('.modal-title').text('Añadir Post Nuevo');
	 $('#postsFormulario').attr('action',base_url+'/entradas/agregar');
	 $('#postsFormulario')[0].reset();
	 //actualizarPersianaDeEstados();
     });

     $("#btnGuardarPost").click(function()
     {
	  var url = $('#postsFormulario').attr('action');
	  //alert(url)
	  //var extranjero="";
	  //   if($("#extranjero").is(':checked'))
	  //	  {
	  //	       extranjero="1";
	  //	  }
	  //	  else
	  //	  {
	  //	       extranjero="0";
	  //	  }
	  var data=
	  {
	       "id":$('#txtIde').val(),
	       "titulo"            :$('#txtTituloPost').val(),
	       "contenido"         :$('#txtContenido').val(),
	       "autor"             :$('#txtAutor').val(),
	        //"imagen"            :$('#imagenPost').val(),
		//	    "id_estado"   :$('#cmbEstados').val(),
		//	    "id_municipio":$('#cmbMunicipios').val(),
		//	    "id_parroquia":$('#cmbParroquias').val(),
		//	    "sexo"        :$('input:radio[name=sexo]:checked').val(),
		//	    "extranjero"  :extranjero
	  }

	  //var titulo    = $('input[name=txtTituloPost]');
	  //var contenido = $('textarea[name=txtContenido]');
	  //var autor     = $('input[name=txtAutor]');
	  //alert(data.titulo);
	  //alert(data.contenido);
	  //alert(data.autor);
	  //alert(data.imagenPost);
	  //var id_estado   = $('select[name=cmbEstados]');
	  //var id_municipio= $('select[name=cmbMunicipios]');
	  //var id_parroquia= $('select[name=cmbParroquias]');
          //
	  //var sexo=$('input[name=sexo]');
          //
	  //var resultado='';
          //
	  //if(empleado.val()=='')
	  //{
	  //       empleado.parent().parent().addClass('has-error');
	  //}
	  //else
	  //{
	  //       empleado.parent().parent().removeClass('has-error');
	  //       resultado += '5';
	  //}
	  //if(direccion.val()=='')
	  //{
	  //       direccion.parent().parent().addClass('has-error');
	  //}
	  //else
	  //{
	  //       direccion.parent().parent().removeClass('has-error');
	  //       resultado += '4';
	  //}
	  //if(id_estado.val()=='0')
	  //{
	  //       id_estado.parent().parent().addClass('has-error');
	  //}
	  //else
	  //{
	  //       id_estado.parent().parent().removeClass('has-error');
	  //	 resultado += '3';
	  //}
	  //if(id_municipio.val() == null)
	  //{
	  //	 vacio=true;
	  //	 id_municipio.parent().parent().addClass('has-error');
	  //}
	  //else
	  //{
	  //alert('Con información');
	  //id_municipio.parent().parent().removeClass('has-error');
	  // resultado += '2';
	  //}	
	  //if(id_parroquia.val() == null)
	  //{
	  //       vacio=true;
	  //       id_parroquia.parent().parent().addClass('has-error');
	  //}
	  //else
	  //{
	  //       id_parroquia.parent().parent().removeClass('has-error');
	  //       resultado += '1';
	  //}	
	  //if(resultado=='54321')
	  //{
	  $.ajax(
	  {
	       url:url,
	       type:'ajax',
	       method:'post',
	       dataType:'json',
	       data:data,
	       success:function(response)
	       {
		    if(response["grabado"]===true)
		    {
			 if(response["tipo"]==="incorporacion")
			 {
			      $('.alert-success').html('Registro guardado exitosamente!').fadeIn().delay(3000).fadeOut('slow');
			 }
			 else
			 {
			      $('.alert-info').html('Registro actualizacdo exitosamente!').fadeIn().delay(3000).fadeOut('slow');								
			 }
			 $('#ventanaModalRegistrarPosts').modal('hide');
			 $('#table_posts').DataTable().ajax.reload();
			 $('#postsFormulario')[0].reset();
		    }
		    else
		    {
			  alert(' ... Error al Incluir registro Chequee la respuesta del Controlador ...');
		    }
	       },
	       error:function()
	       {
		      alert('!!! ...Error del Ajax ... !!!');
	       }
	  });
//			}
//			else
//			{
//				alert(resultado);				
//				alert('Hay un campo vacío o con un valor no válido');
//			}
     });

     //La Edicion de Registros: (Este es el tbody de la tabla)
     $('#lista_de_posts').on('click','.editar-item',function(){
	  var id=$(this).attr('id_a_editar');
	  //alert(id);
	  $('#ventanaModalRegistrarPosts').modal('show');
	  $('#ventanaModalRegistrarPosts').find('.modal-title').text(' Editar Post ');
	  $('#postsFormulario').attr('action',base_url+'/entradas/actualizar');
	  $.ajax(
	  {
	       type:'ajax',
	       url:base_url+'/entradas/buscar',
	       method:'get',
	       data:{id:id},
	       dataType:'json',
	       success:function(data)
	       {
		    //alert(data.id);
		    //Limpio el formulario para llenarlo con
		    //lo que retorne el ajax de búsqueda
		    $('#postsFormulario')[0].reset();
		    //alert(data.titulo);
		    $('input[name=txtIde]').val(data.id);
		    $('input[name=txtTituloPost]').val(data.titulo);
		    $('textarea[name=txtContenido]').val(data.contenido);
		    $('input[name=txtAutor]').val(data.autor);
		    //actualizarPersianaDeEstados(data.id_estado, data.id_municipio, data.id_parroquia);
		    //$('radio[name=sexo]').val(data.sexo);
		    //if(data.sexo=='M')
		    //{
		    //     $("#sexom").attr('checked', true);
		    //     $("#sexof").attr('checked', false);
		    //}
		    //else if(data.sexo=='F')
		    //{
		    //     $("#sexof").attr('checked', true);
		    //     $("#sexom").attr('checked', false);
		    //}
		    //else
		    //{
		    //     alert('no sabe');
		    //     $("#sexof").attr('checked', false);
		    //     $("#sexom").attr('checked', false);
		    //}
		    //$('checkbox[name=extranjero]').val(data.extranjero);
		    //$('#extranjero').attr('value', data.extranjero);  
		    //if(data.extranjero==1)
		    //{
		    //     $('#extranjero').attr('checked', true);  
		    //}
		    //else
		    //{
		    //     $('#extranjero').attr('checked', false);  
		    //}
	       },
	       error:function()
	       {
		    alert(' !!! ...Error al ejecutar el Ajax de Búsqueda de Registro para su Edición  ... !!!');
	       }
	  });
     });

     //La Eliminación de Registros:
     $('#lista_de_posts').on('click','.eliminar-item', function(){
	  var id=$(this).attr('id_a_eliminar');
	  var titulo_a_eliminar = $(this).attr('titulo_a_eliminar');

	  $('#ventanaModalEliminar').find('.modal-title').text('Confirmar Eliminación del Registro ' + titulo_a_eliminar);
	  $('#ventanaModalEliminar').modal('show');

	  //No c ahorita para que es el unbind()
	  $('#btnEliminar').unbind().click(function()
	  {
	       $.ajax(
	       {
		    type:"ajax",
		    method:"post",
		    url:base_url+"/entradas/eliminar",
		    data:{id:id},
		    success:function(response)
		    {
			 //alert(response);
			 if(response=='true')
			 {
			      $('#ventanaModalEliminar').modal('hide');
			      $('#table_posts').DataTable().ajax.reload();
			      $('.alert-danger').html('... Registro eliminado exitosamente ...!').fadeIn().delay(3000).fadeOut('slow');
			 }
			 else
			 {
			       alert(' ... Error al Eliminar registro ...');
			 }					  	
			 //$('#table_posts').DataTable().ajax.reload();
		    },
		    error: function(response)
		    {
		      console.log(response);
		      alert(' !!! ...Error al ejecutar el Ajax de Eliminación  ... !!!');
		    }
	       });
	  });
     });

     //Edicion de la Imagen del Registro:
     $('#lista_de_posts').on('click','.imagen-para-el-item',function(){
	  var id=$(this).attr('id_post');
	  var titulo=$(this).attr('post_titulo');
	  alert(id);
	  alert(titulo);
	  //alert('base_url='+base_url)
	  $(location).attr('href',base_url+'/entradas/asociar_imagen/'+id+'/'+titulo);
	  //	var titulo_post=$(this).attr('post_titulo');
	      //alert(id);
	  //	$('input[name=txtIde]').val(id);
	  //	$('#ventanaModalImagenPost').modal('show');
	  //	$('#ventanaModalImagenPost').find('.modal-title').text(' Imagen del Post ' + titulo_post);
	  //	$('#postsImagenFormulario').attr('action',base_url+'/posts/asociar_imagen/'); //Pendiente Ruteo
	  //lo que retorne el ajax de búsqueda
	  //$('#postsFormulario')[0].reset();
     });

/*
     $("#btnVerImagenAsociada").click(function()
     {
	       var id=$('#txtIde').val();
	       var titulo=$('#txtTituloPost').val();
	       alert('Esta es Aide='+id);
	       alert('Y Este es el Título='+titulo);
	       $(location).attr('href',base_url+'/entradas/ver_imagen_asociada/'+id+'/'+titulo);
	       $.ajax
	       (
		    {
			 type:'ajax',
			 url:base_url+'entradas/buscar_imagen_asociada',
			 method:'get',
			 data:{id:id},
			 dataType:'json',
			 success:function(data)
			 {
			 alert(data.id);
			 if(data.sexo=='M')
			 {
			 }
			 else
			 {
			 }
		    }
		    error:function()
		    {
				alert(' !!! ...Error al ejecutar el Ajax de Búsqueda de Registro  ... !!!');
     });
*/
/*
     $("#titulo_imagen").click(function()
     {
		     alert('base_url=');
	  //var url = $('#postsImagenFormulario').attr('action');
	  //alert(url);
	  /*
	  var data=
	  {
	       "imagen":$('#imagenPost').val(),
	       "nombre":'luis',
	  }
	  alert(data.imagen);
	  alert(data.nombre);
	  alert(url);
          $.ajax(
	  {
	       url:url,
	       type:'ajax',
	       methos:'post',
	       dataType:'text',
	       data:data,
	       success:function(response)
	       {
		    alert(response);
	       },
	       error:function()
	       {
		    alert('Eeeeeee Error en el ajax ...!');
	       }
	  });
     });
*/


	     function actualizarPersianaDeEstados(idestado, idmunicipio, idparroquia)
	     {
		   var rutaEstados=base_url+'/estados';

		    $.post
			(
			     rutaEstados,
			     function(data)
			     {
				  //alert(data);
				  $('#cmbEstados').empty();
				  ////$('#cmbEstados').append('<option value=0>Seleccione un estado</option>');
				  var estados = JSON.parse(data);
				  if(idestado===undefined)
				  {
				       $.each(estados, function(i, item)
				       {
					     $('#cmbEstados').append('<option value=' + item.id + '>' + item.estado + '</option>');
				       });
				       idestado=$('#cmbEstados').val();
				  }
				  else
				  {
				       $.each(estados, function(i, item)
				       {
					    if(item.id==idestado)
					    {
					     $('#cmbEstados').append('<option value=' + item.id + ' selected>' + item.estado + '</option>');
					    }
					    else
					    {
					     $('#cmbEstados').append('<option value=' + item.id + '>' + item.estado + '</option>');
					    }
				       });
				       //alert('idmunicipio es='+idmunicipio);
				  }
				  actualizarPersianaDeMunicipios(idestado, idmunicipio, idparroquia);
			     }
			);
	     }
		function actualizarPersianaDeMunicipios(idestado, idmunicipio, idparroquia)
		{
		    var rutaMunicipiosPorEstado=base_url+'/municipios/listarMunicipiosPorEstado';
		    var txtIdeEstado=idestado;
		    //var txtIdeEstado=$("#cmbEstados").val();
		    $.post
		    (
			rutaMunicipiosPorEstado,
			{txtIdeEstado: txtIdeEstado},
			function(data)
			{
			   //alert(data);
			   var municipios = JSON.parse(data);
			   $('#cmbMunicipios').empty();
			   if(idmunicipio===undefined)
			   {
				$.each(municipios, function(i, item)
				{
				     $('#cmbMunicipios').append('<option value=' + item.id + '>' + item.municipio + '</option>');
				});
				idmunicipio=$('#cmbMunicipios').val();
			   }
			   else
			   {
				  $.each(municipios, function(i, item)
				  {
				       if(item.id==idmunicipio)
				       {
					  //console.log(item.id);
					  //console.log("-");
					  //console.log(idmunicipio);
					  $('#cmbMunicipios').append('<option value=' + item.id + ' selected>' + item.municipio + '</option>');
				       }
				       else
				       {
					  //console.log(item.id);
					  //console.log("/");
					  //console.log(idmunicipio);
					  $('#cmbMunicipios').append('<option value=' + item.id + '>' + item.municipio + '</option>');
				       }
				  });
			   }
			   actualizarPersianaDeParroquias(idmunicipio, idparroquia);
			 }
		    );
	       };
		function actualizarPersianaDeParroquias(idmunicipio, idparroquia)
		{
		    var rutaParroquiasPorMunicipio=base_url+'/parroquias/listarParroquiasPorMunicipio';
		    var txtIdMunicipio=idmunicipio;
		    //txtIdMunicipio=$("#cmbMunicipios").val();
		    $.post
		    (
			rutaParroquiasPorMunicipio,
			{txtIdMunicipio: txtIdMunicipio},
			function(data)
			{
			   //alert(data);
			   var parroquias = JSON.parse(data);
			   $('#cmbParroquias').empty();
			   if(idparroquia===undefined)
			   {
				$.each(parroquias, function(i, item)
				{
				     $('#cmbParroquias').append('<option value=' + item.id + '>' + item.parroquia + '</option>');
				});
				idparroquia=$('#cmbParroquias').val();
			   }
			   else
			   {
				  $.each(parroquias, function(i, item)
				  {
				       if(item.id==idparroquia)
				       {
					  $('#cmbParroquias').append('<option value=' + item.id + ' selected>' + item.parroquia + '</option>');
				       }
				       else
				       {
					  $('#cmbParroquias').append('<option value=' + item.id + '>' + item.parroquia + '</option>');
				       }
				  });
			   }
			 }
		    );
	       };

	       $("#cmbEstados").change(function()
	       {
		    actualizarPersianaDeMunicipios($('#cmbEstados').val(), undefined, undefined);
	       });
	       $("#cmbMunicipios").change(function()
	       {
		    actualizarPersianaDeParroquias($('#cmbMunicipios').val(), undefined);
	       });

$('#btnVerificador').click(function()
{
	// Verificar el Radio Buton Seleccionado y mostrar su valor
	alert("El Sexo es : " + $('input:radio[name=sexo]:checked').val());
	// Verificar Si el campo checkbox está Seleccionado y si es así su valor
	if($("#extranjero").is(':checked'))
	{
		alert('La Persona es extranjera');
	}
	else
	{
		alert('La Persona no es extranjera');
	}

});
/*Rutina para, mediante el evento click de un botón cambiar el
atributo checked de un control checkbox
    $("#checkbox_activar").click(function() {  
        $("#checkbox").attr('checked', true);  
    });  
  
    $("#checkbox_desactivar").click(function() {  
        $("#checkbox").attr('checked', false);  
    });  
*/

/*
	  $("#cmbMunicipios").change(function()
	  {
	       alert('actualizarPersianaDeParroquias()');
	  });
*/

/* ************************************** */
		function listarEmpleados_2()
		{
			//var base="<?= base_url(); ?>" + "/empleado/listar";
			var base=base_url+"/empleado/listar";
			//alert(base);
			$.ajax(
			{
				//url:"/empleado/listarempleados",
				url:base,
				type:"ajax",
				//async:false,
				dataType:'json',				
				beforeSend:function()
				{
					//alert(' ... procesando la  petición, por favor espere un momento ...');
				},
				success:function(data)
				{
					var html=''
					var i
					for (i=0; i<data.length; i++)
					{
						html +=
						'<tr>'+
							'<td>'+data[i].id+'</td>'+
							'<td>'+data[i].nombre+'</td>'+
							'<td>'+data[i].direccion+'</td>'+
							'<td>'+data[i].creado_en+'</td>'+
							'<td>'+
								'<a href="javascript:;" class="btn btn-info editar-item" id_a_editar="'+data[i].id+'">Editar</a>'+
								'<a href="javascript:;" class="btn btn-danger eliminar-item" id_a_eliminar="'+data[i].id+'" nombre_a_eliminar="'+data[i].nombre+'">Eliminar</a>'+
							'</td>'+
						'</tr>'
					}
					$('#lista_de_empleado').html(html);
        			//Activacion del DataTable
					//1.- Ordenado de Forma Descendente según la primera columna
/*					if ($.fn.dataTable.isDataTable('#table_empleado'))
					{
						$('#table_empleado').destroy();
					}	*/			
        			//$('#table_empleado').DataTable(
        			//{
/*				      "responsive": true,
				      "autoWidth": false,
				      "order":[[0,"desc"]],
				      "language":
				      {                                 
				      //"url": "<?= base_url(); ?>/lang_datatable/Spanish.json"
				      //"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
					  "sProcessing":    "Procesando...",
					  "sLengthMenu":    "Mostrar _MENU_ registros",
					  "sZeroRecords":   "No se encontraron resultados",
					  "sEmptyTable":    "Ningún dato disponible en esta tabla",
					  "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					  "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
					  "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
					  "sInfoPostFix":   "",
					  "sSearch":        "BuscarZZZZ:",
					  "sUrl":           "",
					  "sInfoThousands":  ",",
					  "sLoadingRecords": "Cargando...",
					  "oPaginate": {
					    "sFirst":    "Primero",
					      "sLast":    "Último",
					      "sNext":    "Siguiente",
					      "sPrevious": "Anterior"
					    },
					  "oAria": {
					    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
					    }
				      },
				      "columnDefs": 
				      [
				    		{
				    	  	"targets": [ 0 ],
				    	  	"visible": false,
				    	  	"searchable": false
				    		}
				      ]*/
					//});
				},
				error:function()
				{
					alert('No le llego a la BD');
				}
			})
		}
/* ************************************** */		
	});
