<?php

namespace App\Validaciones;

/**
 * 
 */
class MisReglas
{
	
	function mi_verificacion_de_fecha(string $str, string &$error = null) : bool
	{
		if($str < date('Y-m-d'))
		{
			return false;
		}
		return true;
	}
}