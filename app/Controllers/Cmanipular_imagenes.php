<?php namespace App\Controllers;

/*PARA CUSTOMIZAR LOS MENSAJES DEL VALIDADOR:
1.- Copiar la carpeta:
/var/www/html/ci4/system/Language/en
/var/www/html/ci4/system/Language/sp (por ejemplo)

2.- En la carpeta  /var/www/html/ci4/system/Language/sp 
En el archivo Validation.php Traducir los mensajes que están en inglés

3.- en la carpeta /var/www/html/ci4/app/Config:
Cambiar en App.php la variable $defaultLocale = 'sp'
en la sección Default Local (línea 72 aprox.).
*/

/* WRITEPATH se configura en Config/Paths.php */

class Cmanipular_imagenes extends BaseController
{
     public function index()
     {
	  /* Invoco la Librería para Validar Formularios */
	  helper(['form','image']);
	  $error=true;
	  $data[]='';

	  if ($this->request->getMethod() == 'post')
	  {
	       $fecha=date('Y-m-d');
	       /* Estas reglas de validación pueden definirse fuera del condicional */
	       $reglas_de_la_imagen =
	       [
		      'miImagen' =>
		      [
			      'rules' => 'uploaded[miImagen]|ext_in[miImagen,jpg,png,jpeg]',
			      'label' => 'La Imagen'
		      ]
	       ];
	       //var_dump($reglas_de_la_imagen);die();
	       //Verifico si la imagen pasa las validaciones:
	       if($this->validate($reglas_de_la_imagen))
	       {
	       	//Defino la Ruta para donde se va a mover la Imagen
		    $ruta = WRITEPATH . 'imagenes/manipuladas/';
		    $ruta_para_mostrar = '/uploads/imagenes/manipuladas/';		    
		    //// xxxx $ruta = '/uploads/imagenes/manipuladas/';
		    //Obtengo la imagen seleccionada y la bautizo
		    $imagen_seleccionada = $this->request->getFile('miImagen');

		    $imageService=\Config\Services::image();
		    //var_dump($ruta);die();
		    if($imagen_seleccionada->isValid() && !$imagen_seleccionada->hasMoved())
		    {
			 $imagen_seleccionada->move($ruta);
			 $imagen_seleccionada_movida = $imagen_seleccionada->getName();
			 $data['imagen']=$imagen_seleccionada_movida;
 			 $data['original'] = base_url() . $ruta_para_mostrar . $imagen_seleccionada_movida;

			 $rutaDondeGuardarImagen = $ruta . 'thumbs' . '/';
			 $this ->manipular_imagen($ruta, 'thumbs', $imagen_seleccionada_movida, $imageService, $rutaDondeGuardarImagen);
 			 $data['carpetas'][] = base_url() . $ruta_para_mostrar . 'thumbs' . '/' . $imagen_seleccionada_movida;
/* ME DA ERROR DE ESCRITURA
			 $rutaDondeGuardarImagen = $ruta . 'flips' . '/';
			 die($rutaDondeGuardarImagen)
			 $this ->manipular_imagen($ruta, 'flips', $imagen_seleccionada_movida, $imageService, $rutaDondeGuardarImagen);
 			 $data['carpetas'][] = base_url() . $ruta_para_mostrar . 'flips' . '/' . $imagen_seleccionada_movida;
*/
		    }
		    else
		    {
				die('El archivo ya se movió o no tiene un formato válido ... ');
		    }
	      }
	  }
	  //return redirect()->('ruta');
	  return view('layout/header.php') .
	  view('layout/menu.php') .
	  view('estructura/uploads/vmanipular_imagenes', $data) .
	  view('estructura/footer_full.php');	
    }
    public function manipular_imagen($ruta, $carpeta, $imagen_seleccionada_movida,$imageService, $rutaDondeGuardarImagen)
    {
	  //$rutaDondeGuardarImagen = $ruta . '/' . $carpeta;
	  $rutaDondeGuardarImagen = $rutaDondeGuardarImagen;
	  /*
	  Ojo con esto porque no crea la carpeta 
	  con estos permisos, los crea 700t, lo hice manualmente
	  if(!file_exists($path . 'thumbs/'))
	  {
	   mkdir($path . 'thumbs/', 755);
	  }
	  */
	  $imagen_movida=$ruta . $imagen_seleccionada_movida;
	  $imageService->withFile($imagen_movida);
	  switch($carpeta)
	  {
	       case 'thumbs':
		    $imageService->fit(150, 150);
		    break;
	       case 'flip':
		    $imageService->flip('horizontal');
		    break;
	  }
	  //echo($rutaDondeGuardarImagen . '/' . $imagen_seleccionada_movida);die();
	  //return $imageService->save($ruta . $carpeta . $imagen_seleccionada_movida);
	  return $imageService->save($rutaDondeGuardarImagen . '/' . $imagen_seleccionada_movida);	  
	  /*
	  Otras opciones de fit son:
	  ->fit(150, 150, 'top-left')
	  ->fit(150, 150, 'top-right')
	  ->fit(150, 150, 'bottom-left')
	  ->fit(150, 150, 'bottom-right')
	  */
    }
   //--------------------------------------------------------------------
}
