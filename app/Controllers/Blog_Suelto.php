<?php namespace App\Controllers;

use App\Models\BlogModel;
use App\Models\CustomModel;

class Blog_Suelto extends BaseController
{
	public function index()
	{
	     $blogModel = new BlogModel();
	     $post = $blogModel->orderBy('post_id','desc')->findAll();
	       $data = 
		[
			'meta_title' => 'Codeigniter 4 Blog',
			'title' => 'Estos son mis blogs',
			'posts' => $post,
		   ];
		    return view('layout/header') .
		    view('layout/menu') .
		    view('header_suelto') .
		    view('blog_suelto', $data) .
		    view('footer_suelto') .
		    view('estructura/footer_full');
	}
	public function post($id)
	{
	       $blogModel = new BlogModel();
	       $post = $blogModel->find($id);
	       if($post)
	       {
		     $data = 
		     [
			'meta_title' => $post['post_title'],
			'title' => $post['post_title'],
			'post' => $post,
		    ];
	       }		
	       else
	       {
		     $data = 
		     [
			'meta_title' => 'Codeigniter 4 Blog',
			'title' => 'Esta es la Página Single Post de mi blog'
		    ];
	       }
	       return view('layout/header') .
	       view('layout/menu') .
	       view('header_suelto') .
		view('single_post_suelto', $data) .
	       view('footer_suelto') .
	       view('estructura/footer_full');
	}
	public function new()
	{
	  if($this->request->getMethod() == 'post')
	  {
	       $blogModel = new BlogModel();
	       $blogModel->save($_POST);
	       //var_dump($_POST);die();
	  }
	  $data = 
	  [
	      'meta_title' => 'Codeigniter 4 Blog',
	      'title' => 'Agregar Blogs',
	  ];
	  return view('layout/header') .
	  view('layout/menu') .
	  view('header_suelto') .
	  view('new_post', $data) .
	  view('footer_suelto') .
	  view('estructura/footer_full');
	}
	public function delete($id)
	{
	     $blogModel = new BlogModel();
	     $post = $blogModel->find($id);
	     if($post)
	     {
		  //Por Ahora y Por Siempre:
		  $data=
		    [
		       'title' => 'Eliminación del Regitro',
		       'aide'  => $id,
		    ];
		    return view('layout/header') .
		    view('layout/menu') .
		    view('header_suelto') .
		    view('delete_post', $data) .
		    view('footer_suelto') .
		    view('estructura/footer_full');
		  //$blogModel->delete($id);
		  //return redirect()->to('/blog_Suelto');

	     }
	}
	public function editar($id)
	{
	       $blogModel = new BlogModel();
	       $post = $blogModel->find($id);
	       if($post)
	       {
		     $data = 
		     [
			'meta_title' => $post['post_title'],
			'title' => $post['post_title'],
			'post' => $post,
		    ];
	       }		
	       if($this->request->getMethod() == 'post')
	       {
		    $blogModel = new BlogModel();

		    //OJO:
		    $_POST['post_id'] = $id;

		    //var_dump($_POST);die();
		    $post = $blogModel->save($_POST);
		    return redirect()->to('/blog_Suelto');
	       }
	       return view('layout/header') .
	       view('layout/menu') .
	       view('header_suelto') .
	       view('edit_post', $data) .
	       view('footer_suelto') .
	       view('estructura/footer_full');
	}
}
