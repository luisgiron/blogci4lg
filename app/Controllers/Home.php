<?php namespace App\Controllers;

use App\Models\PostModel;
use App\Models\PostImagenesModel;
use App\Models\CustomModel;

class Home extends BaseController
{
     public function index()
     {
	  $postModel = new PostModel();
	  $post = $postModel->orderBy('id','desc')->where('activo', 1)->findAll();
	  $imagenesBlogModel = new PostImagenesModel();
	  $imagenes = $imagenesBlogModel->orderBy('id_post','asc')->where('activo', 1)->findAll();
	  $data = 
	  [
	       'posts' => $post,
	       'imagenes' => $imagenes,
	  ];
	  return view('estructura/header') .
	  view('blog/bloghome', $data) .
	  view('estructura/footer_full');
     }
     public function admininicio()
     {
	  /* PARA PODER HACER EL MODULO DE REGISTRO DESDE FUERA
	  $session=session();
	  if (isset($_SESSION['id']))
	  {
	       return
	       view('estructura/header') .
	       view('estructura/menu') .
	       view('adminblog/inicio') . 
	       view('estructura/footer_full');
	  }
	  else
	  {*/
	       return view('estructura/escris') . view('estructura/login');			
	  //}
     }
     public function registrousuario()
     {
	  /*
	  $session=session();
	  if (isset($_SESSION['id']))
	  {
	       return
	       view('estructura/header') .
	       view('estructura/menu') .
	       view('adminblog/registrousuario') . 
	       view('estructura/footer_full');
	  }
	  else
	  {
	   */
	  return
	       view('estructura/header') .
	       view('estructura/escris') .
	       view('adminblog/registrousuario');			
	  //}
     }

	//--------------------------------------------------------------------

}
