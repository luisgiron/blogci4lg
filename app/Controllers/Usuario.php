<?php namespace App\Controllers;

use App\Models\UsuarioModel;

class Usuario extends BaseController
{
	public function index()
	{
		$session=session();
	     //return('que te pasa calabaza ...');
		//return $session->get('id');
		//return $_SESSION['id'];
		if (isset($_SESSION['id']))
		{
			////return 'Sesionado';
			$usuarioModel = new UsuarioModel($db);
			//Muéstrame los borrados también
			$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
			////$usuarios=$usuarioModel->paginate(8);
			////$paginas =$usuarioModel->pager;
			// $usuarios=array('usuarios' => $usuarios,'paginas'=>$paginas);
			$usuarios=array('usuarios' => $usuarios);
			
			return
			view('layout/header.php') .
			view('layout/menu.php') .
			//view('estructura/usuario/usuario_listar.php', $usuarios) . 
			view('estructura/footer_full.php');
		}
		else
		{
			//return ' ... Debe autenticarse con un correo válido primero (ci4.com/login)  ... !!!';
			return view('estructura/escris.php') . view('estructura/login.php');			
		}
	}
	public function __construct()
	{
		helper('form');
	}
	public function agregar()
	{
		return view('estructura/head.php') . view('estructura/usuario_agregar.php') . view('estructura/escris.php') . view('estructura/footer.php');
	}	
	  public function guardar()
	  {
	       $usuarioModel =  new UsuarioModel($db);

	       $request = \Config\Services::request();
	       $clave=$request->getPostGet('clave');
	       $clave=password_hash($clave, PASSWORD_BCRYPT, ['cost' =>10]);
	       //'clave'=>$request->getPostGet('clave')

	       $datos = 
	       [
		    //'id'=>$request->getPostGet('id'),
		    'nombre'=>$request->getPostGet('nombre'),
		    'correo'=>$request->getPostGet('correo'),
		    'clave' =>$clave,
	       ];
	       //print_r($datos);die();
	       //Verificamos que se guarde el Registro
	       if($usuarioModel->save($datos)===false)
	       {
		   //$dataErrores=['errores' =>$usuarioModel->errors()];
		   //var_dump($dataErrores);
		   //Muestro los mensajes configurados para cada error en una Vista:
		   //return view('estructura/actualizarUsuario',$dataErrores);
		    $mensaje=0;
	       }	
	       else
	       {
		     $mensaje=1;
	       }
	       /*	$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
	       $usuarios=array('usuarios' => $usuarios);	*/	
	       //return redirect()->to('/usuario');
	       return json_encode($mensaje);
	}
	public function editar()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();
		
		////$id=$request->getPostGet('id');
		//$id=$request->uri->getSegment(4);  //???
		//echo $request->uri->getSegment(4) . ' y ' . $request->uri->getSegment(5);
		//echo $request->uri->getTotalSegments();
/*		$segmentos=$request->uri->getSegments();
		var_dump($segmentos);
		die();*/

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);			
		}

		//var_dump($id);die();
		$usuario=$usuarioModel->find($id);
		$usuario=array('usuario' => $usuario);
		return view('estructura/head').view('estructura/usuario_agregar',$usuario) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}
	public function eliminar()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);  //???			
		}
		//var_dump($id);die();		
		$usuarioModel->delete($id);

		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);
		//var_dump($usuarioEncontrado);
		
		return view('estructura/head.php') . view('estructura/usuario_listar.php', $usuarios) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}
	public function buscar_login()
	{
	  //$mensaje="Esto es una prueba antes de implementar el ajax";
	  //return json_encode($mensaje);		
	  //echo json_encode('stts');
	  //Libreria para halar los datos vía Get o Post
	  $request = \Config\Services::request();
	  if ($request->getPostGet('login'))
	  {
	       //$mensaje='Llego Aide';
	       $db = \Config\Database::connect();

	       $login=$request->getPostGet('login');
	       $strQuery="SELECT ";
	       $strQuery.="id";
	       $strQuery.=",nombre";
	       $strQuery.=" FROM ";
	       $strQuery.=" usuario";
	       //$strQuery.=" WHERE correo='$login';";
	       $strQuery.=" WHERE nombre='$login';";

	       $resultado=$db->query($strQuery);
	       $resultado=$resultado->getRowArray();

	       //Para debuguear el Query:
	       //$mensaje=$strQuery;
	      
	       //1.- Evaluo si la consulta trajo algo:			
	       if (isset($resultado))
	       {
		    $mensaje=1;
	       }
	       else
	       {
		    $mensaje=0;				
	       }
	  }
	  else
	  {
	       $mensaje='No ha llegado Aide';
	       ////return json_encode($mensaje);			
	  }
	  return json_encode($mensaje);		
	}	
	public function buscarlogin()
	{
	   //Libreria para halar los datos vía Get o Post
	   $request = \Config\Services::request();
	   //$mensaje=$request->getPostGet('login');
	   //$mensaje=$request->getPostGet('clave');
	   //if ($request->getPostGet('login'))
	   if ($request->getPostGet('login') AND $request->getPostGet('clave'))
	   {
	      //$mensaje='Llego Aide y la clave';
	      $db = \Config\Database::connect();

	      $login=$request->getPostGet('login');
	      $clave=$request->getPostGet('clave');

	      $strQuery="SELECT ";
	      $strQuery.="id";
	      $strQuery.=",nombre";
	      $strQuery.=",clave";
	      $strQuery.=" FROM ";
	      $strQuery.=" usuario";
	      //$strQuery.=" WHERE correo='$login';";
	      $strQuery.=" WHERE nombre='$login';";

	      $resultado=$db->query($strQuery);
	      $resultado=$resultado->getRowArray();

	      //Para debuguear el Query:
	      //$mensaje=$strQuery;
			
	      //1.- Evaluo si la consulta trajo algo:			
	      if (isset($resultado))
	      {
		    //Sin Montar nada en Sesion
		    //$id    =$resultado['id'];
		    //$nombre=$resultado['nombre'];

		    //2.- Montar en sesión: id y nombre
		    ////$session = session();							

		    //2.a.- Se Montan las variables una por una en Sesión
		    //$session->set('some_name', 'some_value');

		    //$session->set('id',$resultado['id']);
		    //$session->set('nomb',$resultado['nombre']);

		    ////$id=$session->get('id');
		    ////$nombre=$session->get('nomb');

		    //2.b.- Se Montan las variables en un array
		    ////$s_data=
		    ////[
		    ////	'id'    =>$resultado['id'],
		    ////	'nomb'  =>$resultado['nombre']
		    ////];
		    ////$session->set($s_data);
		    ////$id=$_SESSION['id'];
		    $clavesgbd=$resultado['clave'];
		    //$mensaje=$clavesgbd;
		    if(password_verify($clave, $clavesgbd))
		    {
			 ////$mensaje="Claves Coincidentes";
			 $mensaje['paso']=1;				
			 $mensaje['error']='';				
		    }
		    else
		    {
			 ////$mensaje="Claves No Coincidentes";
			 $mensaje['paso']=0;				
			 $mensaje['error']='Claves No Coincidentes';				
		    }
		    //$mihash=password_hash($clave, PASSWORD_BCRYPT, ['cost' =>10]);
		    ////$mensaje=1;
	       }
	       else
	       {
		    //La consulta no trajo Registros
		    //$mensaje=0;				
		    $mensaje['paso']=0;				
		    $mensaje['error']='No hay registros coincidentes';				
	       }
	       //3.- Ponerle un estilo a la vista
	       //Mostrar los valores montados en sesión

	       //$mensaje=$resultado;
	       return json_encode($mensaje);
      }
      else
      {
	      $mensaje='No ha llegado Aide ni la clave';
	      ////return json_encode($mensaje);			
      }
      return json_encode($mensaje);		
     }	
	//--------------------------------------------------------------------
}
