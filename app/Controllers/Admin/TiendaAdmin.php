<?php namespace App\Controllers\Admin;

use App\Controllers\BaseController;

/* OJISIMO: Si yo le pongo una letra mayúscula en parte del nombre de la Clase, debo escribir esa mayúscula en la url */

class TIendaadmin extends BaseController
{
     public function index()
     {
	  return 'Estoy en la Tienda Administrador';
     }
     public function producto($tipo='pc', $marca='hp')
     {
	  return "<h2>Este es un Admin producto para $tipo y para $marca</h2>";
     }
}
