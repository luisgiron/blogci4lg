<?php namespace App\Controllers;

#use CodeIgniter\Controller;
use App\Models\UsuarioModel;

class Mihome extends BaseController
{
	public function __construct()
	{
		helper('form');
	}
	public function formulario()
	{
		return view('estructura/head.php') . view('estructura/formulario.php');
	}	
	public function guardarusuario()
	{
		$usuarioModel =  new UsuarioModel($db);

		$request = \Config\Services::request();

		$datos = 
		[
			'id'=>$request->getPostGet('id'),
			'nombre'=>$request->getPostGet('nombre'),
			'correo'=>$request->getPostGet('correo')
		];
		//print_r($datos);die();
		//Verificamos que se guarde el Registro
		if($usuarioModel->save($datos)===false)
		{
			$dataErrores=['errores' =>$usuarioModel->errors()];
			//var_dump($dataErrores);
			//Muestro los mensajes configurados para cada error en una Vista:
			return view('estructura/actualizarUsuario',$dataErrores);
		}	

/*		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);	*/	
		return redirect()->to('/mihome');
	}
	public function editarusuario()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();
		
		////$id=$request->getPostGet('id');
		//$id=$request->uri->getSegment(4);  //???
		//echo $request->uri->getSegment(4) . ' y ' . $request->uri->getSegment(5);
		//echo $request->uri->getTotalSegments();
/*		$segmentos=$request->uri->getSegments();
		var_dump($segmentos);
		die();*/

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);			
		}

		//var_dump($id);die();
		$usuario=$usuarioModel->find($id);
		$usuario=array('usuario' => $usuario);
		return view('estructura/head').view('estructura/formulario',$usuario);
	}
	public function eliminarusuario()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);  //???			
		}
		//var_dump($id);die();		
		$usuarioModel->delete($id);

		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);
		//var_dump($usuarioEncontrado);
		
		return view('estructura/head.php') . view('estructura/body.php', $usuarios);
	}	
	public function index()
	{
		//return view('welcome_message');
	
		$usuarioModel = new UsuarioModel($db);
		//Inserción De Registros:
/*		$datos=
		[
			'nombre'=>'Marisol'
			,'correo'=>'marisol@gmail.com'
		];
		$usuarioModel->insert($datos);*/
		//Actualización De Registros:
/*		$datos=
		[
			'nombre'=>'Beltran'
			,'correo'=>'beltran@hotmail.com'
		];
		$usuarioModel->update('3',$datos);*/
		//Actualización Masiva De Registros:
/*		$datos=
		[
			'correo'=>'yoldann@hotmail.com'
		];
		$usuarioModel->update([1,2],$datos);
*/

		//Actualización con Set:
/*		$usuarioModel->whereIn('id',[1,2,3])
		->set(['correo'=>'correo@asapo.gob.ve'])
		->update();
*/
		//Utilizando SAVE
/*		$datos=
		[
			'id'=>'3'
			,'nombre'=>'Luis'
			,'correo'=>'luis@gmail.com'
		];
		$usuarioModel->save($datos);*/

		//Borrar registros:
		//$usuarioModel->delete(18);
		//$usuarioModel->where('nombre', 'Luis Otra vez')->delete();
		//$usuarioModel->purgeDeleted();

		//Para verificar la validación de CodeIgniter:
		/*$datos=
		[
			'id'=>'',
			'nombre'=>'domingo Robinson Crusoe',
			'correo'=>'domingo@correo.com'*/
		//];
/*		if($usuarioModel->save($datos)===false)
		{
			$dataErrores=['errores' =>$usuarioModel->errors()];
			//var_dump($dataErrores);
			//Muestro los mensajes configurados para cada error en una Vista:
			return view('estructura/actualizarUsuario',$dataErrores);
		}*/

		//Muéstrame todo
		//$usuario=$usuarioModel->findAll();

		//Muéstrame los IDs 1 y 2
		// $usuario=$usuarioModel->find([1,2]);
		
		//Muéstrame donde ..
		// $usuario=$usuarioModel->where('nombre','Maria')->findAll();

		//Muéstrame 3 desde el 2 (limit con offset)
		// $usuario=$usuarioModel->findAll(3,2);
		//Muéstrame Sólo los Borrados
		//$usuario=$usuarioModel->onlyDeleted()->findAll();
		//Muéstrame los borrados también
		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);
		//var_dump($usuarioEncontrado);
		
		return view('estructura/head.php') . view('estructura/body.php', $usuarios);
	}

	//--------------------------------------------------------------------

}
