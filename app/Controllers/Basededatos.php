<?php namespace App\Controllers;

class Basededatos extends BaseController
{
	public function __construct()
	{

	}
	public function index()
	{
		echo 'Debes indicar un método para este Controlador';
	}
	public function cedulas_resultObject()
	{
		//return view('estructura/pruebabd.php');
		$db = \Config\Database::connect();

		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',nacionalidad';
		$strQuery.=',cedula';
		$strQuery.=',primerapellido';
		$strQuery.=',segundoapellido';
		$strQuery.=',primernombre';
		$strQuery.=',segundonombre';
		$strQuery.=' FROM ';
		$strQuery.=' cedulas;';

		// $query = $db->query('SELECT id, nacionalidad, cedula, primernombre FROM cedulas');
		$query = $db->query($strQuery);
		$cedulas = $query->getResult();

		foreach ($cedulas as $cedula)
		{
		        echo $cedula->id;
		        echo $cedula->nacionalidad;
		        echo $cedula->cedula;
		        echo $cedula->primerapellido;
		        echo $cedula->segundoapellido;		        		        
		        echo $cedula->primernombre;
		        echo $cedula->segundonombre;
		        echo '<br>';		        
		}

		echo 'Total Results: ' . count($cedulas);		
	}
	public function cedulas_resultArray()
	{
		//return view('estructura/pruebabd.php');
		$db = \Config\Database::connect();

		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',nacionalidad';
		$strQuery.=',cedula';
		$strQuery.=',primerapellido';
		$strQuery.=',segundoapellido';
		$strQuery.=',primernombre';
		$strQuery.=',segundonombre';
		$strQuery.=' FROM ';
		$strQuery.=' cedulas;';

		// $query = $db->query('SELECT id, nacionalidad, cedula, primernombre FROM cedulas');
		$query = $db->query($strQuery);
		$cedulas = $query->getResultArray();

		foreach ($cedulas as $cedula)
		{
		        echo $cedula['id'];
		        echo $cedula['nacionalidad'];
		        echo $cedula['cedula'];
		        echo $cedula['primerapellido'];
		        echo $cedula['segundoapellido'];		        		        
		        echo $cedula['primernombre'];
		        echo $cedula['segundonombre'];
		        echo '<br>';		        
		}

		echo 'Total Results: ' . count($cedulas);		
	}
	public function cedulas_single()
	{
		//return view('estructura/pruebabd.php');
		$db = \Config\Database::connect();

		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',nacionalidad';
		$strQuery.=',cedula';
		$strQuery.=',primerapellido';
		$strQuery.=',segundoapellido';
		$strQuery.=',primernombre';
		$strQuery.=',segundonombre';
		$strQuery.=' FROM ';
		$strQuery.=' cedulas';
		$strQuery.=' LIMIT 1;';

		$query = $db->query($strQuery);
		$cedula = $query->getRowArray();

		echo $cedula['cedula'];
	}	
}
