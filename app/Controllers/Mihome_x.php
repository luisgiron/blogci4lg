<?php namespace App\Controllers;

#use CodeIgniter\Controller;
use App\Models\UsuarioModel;

class Mihome extends BaseController
{
	public function index()
	{
		$usuarioModel = new UsuarioModel($db);
		//Muéstrame los borrados también
		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		////$usuarios=$usuarioModel->paginate(8);
		////$paginas =$usuarioModel->pager;
		// $usuarios=array('usuarios' => $usuarios,'paginas'=>$paginas);
		$usuarios=array('usuarios' => $usuarios);
		
		return view('estructura/head.php') . view('estructura/usuario_listar.php', $usuarios) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}
	public function __construct()
	{
		helper('form');
	}
	public function formulario()
	{
		return view('estructura/head.php') . view('estructura/formulario.php') . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}	
	public function guardarusuario()
	{
		$usuarioModel =  new UsuarioModel($db);

		$request = \Config\Services::request();

		$datos = 
		[
			'id'=>$request->getPostGet('id'),
			'nombre'=>$request->getPostGet('nombre'),
			'correo'=>$request->getPostGet('correo')
		];
		//print_r($datos);die();
		//Verificamos que se guarde el Registro
		if($usuarioModel->save($datos)===false)
		{
			$dataErrores=['errores' =>$usuarioModel->errors()];
			//var_dump($dataErrores);
			//Muestro los mensajes configurados para cada error en una Vista:
			return view('estructura/actualizarUsuario',$dataErrores);
		}	

/*		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);	*/	
		return redirect()->to('/mihome');
	}
	public function editarusuario()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();
		
		////$id=$request->getPostGet('id');
		//$id=$request->uri->getSegment(4);  //???
		//echo $request->uri->getSegment(4) . ' y ' . $request->uri->getSegment(5);
		//echo $request->uri->getTotalSegments();
/*		$segmentos=$request->uri->getSegments();
		var_dump($segmentos);
		die();*/

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);			
		}

		//var_dump($id);die();
		$usuario=$usuarioModel->find($id);
		$usuario=array('usuario' => $usuario);
		return view('estructura/head').view('estructura/formulario',$usuario) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}
	public function eliminarusuario()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);  //???			
		}
		//var_dump($id);die();		
		$usuarioModel->delete($id);

		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);
		//var_dump($usuarioEncontrado);
		
		return view('estructura/head.php') . view('estructura/usuario.php', $usuarios) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}	
	//--------------------------------------------------------------------

}
