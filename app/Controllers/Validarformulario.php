<?php namespace App\Controllers;

/*PARA CUSTOMIZAR LOS MENSAJES DEL VALIDADOR:
1.- Copiar la carpeta:
/var/www/html/ci4/system/Language/en
/var/www/html/ci4/system/Language/sp (por ejemplo)

2.- En la carpeta  /var/www/html/ci4/system/Language/sp 
En el archivo Validation.php Traducir los mensajes que están en inglés

3.- en la carpeta /var/www/html/ci4/app/Config:
Cambiar en App.php la variable $defaultLocale = 'sp'
en la sección Default Local (línea 72 aprox.).
*/

class Validarformulario extends BaseController
{
	public function index()
	{
		/* Invoco la Librería para Validar Formularios */
		helper(['form']);

		/* Lleno el arreglo de las categorías */
		$data=[];
		$data['categorias']=
		[
			'estudiante',
			'docente',
			'director'
		];
		/*
		if ($_POST){
			echo("<pre>");
			print_r($_POST);die();
			 echo "<pre>";
		}
		*/
/*		$reglas_de_validacion = 
		[
			// 'correo' => 'required',
			'correo' => 'required|valid_email',
			'clave'  => 'required|min_length[8]',
			'categoria' => 'required|in_list[estudiante,docente,director]'
		];
*/
		if ($this->request->getMethod() == 'post')
		{
			/* Estas reglas de validación pueden definirse fuera del condicional */
			$reglas_de_validacion = 
			[
				/* Especificación simple de validaciones para un campo: */
				// 'correo' => 'required|valid_email',
				/* Especificación de validaciones para un campo en forma de Array: */
				'correo' =>
				[
					'rules' =>'required|valid_email',
					'label' =>'Dirección de Correo',
					'errors' =>
					[
						'required' => 'Atención, el Renglón Correo es Requerido',
						'valid_email'=>'Por favor, coloque un contenido válido para el correo',
					]
				],
				'clave'  => 'required|min_length[8]',
				'categoria' => 'required|in_list[estudiante,docente,director]',

				'fecha' =>
				[
					'rules' =>'required|mi_verificacion_de_fecha',
//					'rules' =>'required',
					'label' => 'fecha de algo',
					'errors' =>
					[
						'required' => 'Debes especificar una fecha de algo pana mio ... ',
						'mi_verificacion_de_fecha' => 'Especifique una fecha no menor a la actual',
					]
				],
				//1.- Crear una Carpeta (Validaciones, por ejemplo), dentro de app
				//2.- Dentro de esta carpeta crear una archivo (MisReglas.php, por ejemplo)
				//3.- Agregar en el archivo Validation.php Dentro de la Carpeta app/Config
				//    sel Set de Reglas personalizadas en el Array de $ruleSets al final, así:
				//    public $ruleSets = [
				// 	  ...
				// 	  \CodeIgniter\Validation\CreditCardRules::class,
				// 	  \App\Validaciones\MisReglas::class,
			];
			if($this->validate($reglas_de_validacion))
			{
				//Has Algo
				return redirect()->to('/validarformulario/validacionexitosa');
			}
			else
			{
				$data['validacion']=$this->validator;
			}
		}
		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/validacion_de_formulario/formulario.php', $data) .
		view('estructura/footer_full.php');	
	}

	function validacionexitosa()
	{
		$data['resultado']='Exitosamente';
		
		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/validacion_de_formulario/formulario_validado', $data) .
		view('estructura/footer_full.php');	
	}
	//--------------------------------------------------------------------

}
