<?php namespace App\Controllers;

/*PARA CUSTOMIZAR LOS MENSAJES DEL VALIDADOR:
1.- Copiar la carpeta:
/var/www/html/ci4/system/Language/en
/var/www/html/ci4/system/Language/sp (por ejemplo)

2.- En la carpeta  /var/www/html/ci4/system/Language/sp 
En el archivo Validation.php Traducir los mensajes que están en inglés

3.- en la carpeta /var/www/html/ci4/app/Config:
Cambiar en App.php la variable $defaultLocale = 'sp'
en la sección Default Local (línea 72 aprox.).
*/

/* WRITEPATH se configura en Config/Paths.php */

class Cminiatura extends BaseController
{
	public function index()
	{
		/* Invoco la Librería para Validar Formularios */
		helper(['form','image']);
		$error=true;

		if ($this->request->getMethod() == 'post')
		{
			$fecha=date('Y-m-d');
			/* Estas reglas de validación pueden definirse fuera del condicional */
			$reglas_de_la_imagen =
			[
				'miImagen' =>
				[
					'rules' => 'uploaded[miImagen]|ext_in[miImagen,jpg,png,jpeg]',
					'label' => 'La Imagen'
				]
			];
			if($this->validate($reglas_de_la_imagen))
			{
				$path = WRITEPATH . 'imagenes/manipuladas/';
			    $imagen = $this->request->getFile('miImagen');

			    $imageService=\Config\Services::image();

			    if($imagen->isValid() && !$imagen->hasMoved())
			    {
			    	$imagen->move($path);
   				    $fileName = $imagen->getName();
   				    /*Ojo con esto porque no crea la carpeta
   				    con estos permisos, los crea 700t, lo hice manualmente
   				    if(!file_exists($path . 'thumbs/'))
   				    {
   				    	mkdir($path . 'thumbs/', 755);
   				    }
   				    */
   				    $mifile=$path . $fileName;
   				    $imageService->withFile($mifile)
   				    ->fit(150, 150, 'center')
   				    ->save($path . 'thumbs/' .$fileName);
   				    /*
   				    Otras opciones de fit son:
   				    ->fit(150, 150, 'top-left')
   				    ->fit(150, 150, 'top-right')
   				    ->fit(150, 150, 'bottom-left')
   				    ->fit(150, 150, 'bottom-right')
   				    */
			    }
			    else
			    {
			    	die('El archivo ya se movió o no tiene un formato válido ... ');
			    }
		   }
		   else
		   {
			   $msg='La imagen no tiene los requerimientos especificados o no se cargó, favor revise';
		   }
	   		//return redirect()->('ruta');
		   ///return view('layout/header.php') .
		   ///view('layout/menu.php') .
		   ///view('estructura/uploads/subidadearchivosvalidada', $data) .
		   ///view('estructura/footer_full.php');	
      	}
    	return view('layout/header.php') .
    	view('layout/menu.php') .
    	view('estructura/uploads/vminiatura.php') .
    	view('estructura/footer_full.php');	
   }
   //--------------------------------------------------------------------
}
