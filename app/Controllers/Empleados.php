<?php namespace App\Controllers;

class Empleados extends BaseController
{
	public function index()
	{

		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/empleado/empleado_listar.php') .
		view('estructura/footer_full.php');
		//return view('estructura/head.php') . view('estructura/escris.php') . view('estructura/empleado_listar.php') . view('estructura/footer.php');
		//return view('estructura/empleado.php');
	}

	public function listar()
	{
		//echo("Estoy en Evaluaciones");
		$db = \Config\Database::connect();


		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',nombre';
		$strQuery.=',direccion';
		$strQuery.=',creado_en';
		$strQuery.=',actualizado_en';
		$strQuery.=' FROM ';
		$strQuery.=' empleado';
		$strQuery.=" WHERE NOT eliminado";		
		$strQuery.=' ORDER BY id DESC;';

		//echo $strQuery;die();

		$query = $db->query($strQuery);
		$empleados = $query->getResultArray();
		//$empleados = $query->getResult();
		echo json_encode($empleados);

	}

	public function agregar()
	{
		//return "Estoy en empleado/agregar";
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();

		//Verifico si me llegó el dato (ojo falta agregar los demás)
		if ($request->getPostGet('empleado'))
		{
			$nombre      =$request->getPostGet('empleado');
			$direccion   =$request->getPostGet('direccion');
			$id_estado   =$request->getPostGet('id_estado');
			$id_municipio=$request->getPostGet('id_municipio');
			$id_parroquia=$request->getPostGet('id_parroquia');

			$sexo=$request->getPostGet('sexo');
			$extranjero=$request->getPostGet('extranjero');

			$db = \Config\Database::connect();

			$strQuery="INSERT INTO ";
			$strQuery.="empleado (";
			$strQuery.="nombre";
			$strQuery.=",direccion";
			$strQuery.=",sexo";
			$strQuery.=",extranjero";
			$strQuery.=",id_estado";
			$strQuery.=",id_municipio";			
			$strQuery.=",id_parroquia";			
			$strQuery.=") VALUES (";
			$strQuery.="'$nombre'";
			$strQuery.=",'$direccion'";
			$strQuery.=",'$sexo'";
			$strQuery.=",'$extranjero'";
			$strQuery.=",'$id_estado'";
			$strQuery.=",'$id_municipio'";						
			$strQuery.=",'$id_parroquia'";						
			$strQuery.=");";

			///Para debuguear el Query:
			///$mensaje=$strQuery;
			//$mensaje=$id_municipio;

			//$db->query($strQuery);
			//$resultado=$db->affectedRows();		
			$mensaje['grabado']=false;

			if($db->query($strQuery))
			{
				$mensaje['grabado']=true;
			        $mensaje['tipo']='incorporacion';
				//$mensaje=true;
			}
			else
			{
				$mensaje['grabado']=false;
				//$mensaje=false;
			}
		}
		else
		{
			$mensaje['grabado']=false;
		        //$mensaje=false;
			//$mensaje='No Hay Nada'; MEJORAR ESTO
			//$id=$request->uri->getSegment(4);
		}

/*		Esto también debe funcionar
		$data=
		[
			'empleado'=>$nombre,
			'direccion'=>$direccion
		]
		$db->table('mytable')->insert($data);
		$resultado=$db->affectedRows();
*/
		//Esta variable data es para verificar en el depurador que variables están llegando efectivamente
		$data=
		[
			"nombre"      =>$request->getPostGet('empleado'),
			"direccion"   =>$request->getPostGet('direccion'),
			"id_estado"   =>$request->getPostGet('id_estado'),
			"id_municipio"=>$request->getPostGet('id_municipio')
		];
		//return json_encode($data);
		
		//$mensaje=true;
	        //$mensaje['grabado']=false;
		
		return json_encode($mensaje);

	}

	public function agregar_2()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();

		//Verifico si me llegó el dato (ojo falta agregar los demás)
		if ($request->getPostGet('empleado'))
		{
			$nombre      =$request->getPostGet('empleado');
			$direccion   =$request->getPostGet('direccion');
			$id_estado   =$request->getPostGet('id_estado');
			//$id_municipio=$request->getPostGet('id_municipio');

			$db = \Config\Database::connect();

			$strQuery="INSERT INTO ";
			$strQuery.="empleado (";
			$strQuery.="nombre";
			$strQuery.=",direccion";
			$strQuery.=",id_estado";
			//$strQuery.=",id_municipio";			
			$strQuery.=") VALUES (";
			$strQuery.="'$nombre'";
			$strQuery.=",'$direccion'";
			$strQuery.=",'$id_estado'";
			//$strQuery.=",'$id_municipio'";						
			$strQuery.=");";
			//Para debuguear el Query:
			$mensaje=$strQuery;
			//$mensaje=$id_municipio;

		}
		else
		{
			//$mensaje='No Hay Nada'; MEJORAR ESTO
			//$id=$request->uri->getSegment(4);
		}		

/*		Esto también debe funcionar
		$data=
		[
			'empleado'=>$nombre,
			'direccion'=>$direccion
		]
		$db->table('mytable')->insert($data);
		$resultado=$db->affectedRows();
*/
		$db->query($strQuery);
		$resultado=$db->affectedRows();
		$mensaje['grabado']=false;

		if($resultado===1)
		{
			$mensaje['grabado']=true;
		}
		else
		{
			$mensaje['grabado']=false;
		}

		$mensaje['tipo']='incorporacion';

		return json_encode($mensaje);
	}

	public function buscar()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();
		if ($request->getPostGet('id'))
		{
			//$mensaje='Llego Aide';
			$db = \Config\Database::connect();

			$id=$request->getPostGet('id');

			$strQuery="SELECT ";
			$strQuery.="id";
			$strQuery.=",nombre";
			$strQuery.=",direccion";
			$strQuery.=",id_estado";
			$strQuery.=",id_municipio";
			$strQuery.=",id_parroquia";
			$strQuery.=",sexo";
			$strQuery.=",extranjero";
			$strQuery.=" FROM ";
			$strQuery.=" empleado";
			$strQuery.=" WHERE id=$id;";

			//Para debuguear el Query:
			//$mensaje=$strQuery;

			$resultado=$db->query($strQuery);
			$resultado=$resultado->getRowArray();			

			//$query = $db->query($strQuery);
			//$cedula = $query->getRowArray();
			//echo $nombre['nombre'];
			$mensaje=$resultado;
		}
		else
		{
			$mensaje='No ha llegado Aide';
		}

		return json_encode($mensaje);		
	}

	public function actualizar()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();

		if ($request->getPostGet('id'))
		{
			$db = \Config\Database::connect();

			$mensaje['grabado']=false;

			/* Vienen con este nombre según la consulta de la búsqueda en la BD */
			$id          =$request->getPostGet('id');
			$empleado    =$request->getPostGet('empleado');
			$direccion   =$request->getPostGet('direccion');
			$id_estado   =$request->getPostGet('id_estado');
			$id_municipio=$request->getPostGet('id_municipio');
			$id_parroquia=$request->getPostGet('id_parroquia');

			$sexo        =$request->getPostGet('sexo');
			$extranjero  =$request->getPostGet('extranjero');

			$fecha_actualizacion=date('Y-m-d H:i:s');

			$strQuery="UPDATE  ";
			$strQuery.="empleado ";
			$strQuery.="SET ";
			$strQuery.="nombre='$empleado'";
			$strQuery.=",direccion='$direccion' ";
			$strQuery.=",id_estado='$id_estado' ";
			$strQuery.=",id_municipio='$id_municipio' ";
			$strQuery.=",id_parroquia='$id_parroquia' ";
			$strQuery.=",sexo='$sexo' ";
			$strQuery.=",extranjero='$extranjero' ";
			$strQuery.=",actualizado_en='$fecha_actualizacion' ";
			$strQuery.="WHERE id=$id;";

			if($db->query($strQuery))
			{
				$mensaje['grabado']=true;
			        $mensaje['tipo']='actualizacion';
			}
			else
			{
				$mensaje['grabado']=false;
			}
		}
		else
		{
			$mensaje['grabado']=false;			
		}
		///$mensaje['tipo']='actualizacion';

		//Para debuguear el Query:
		////$msjDebug=$strQuery;
		////return json_encode($msjDebug);
							
		return json_encode($mensaje);
	}

	public function eliminar()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();

		if ($request->getPostGet('id'))
		{
			$db = \Config\Database::connect();

			//$mensaje['grabado']=false;

			$id       =$request->getPostGet('id');
			$fecha_actualizacion=date('Y-m-d H:i:s');

			$strQuery="UPDATE  ";
			$strQuery.="empleado ";
			$strQuery.="SET ";
			$strQuery.="eliminado=1";
			$strQuery.=",actualizado_en='$fecha_actualizacion' ";
			$strQuery.="WHERE id=$id;";	

			if($db->query($strQuery))
			{
				$mensaje=true;
			}
			else
			{
				$mensaje=false;
			}
		}
		else
		{
			$mensaje=false;			
		}

		//Descomentar para debuguear el Query:
		//$mensaje=$strQuery;		
		//return json_encode($mensaje);
		//$mensaje="A donde ira";
		return json_encode($mensaje);
	}

	//--------------------------------------------------------------------

}
