<?php namespace App\Controllers;

/*PARA CUSTOMIZAR LOS MENSAJES DEL VALIDADOR:
1.- Copiar la carpeta:
/var/www/html/ci4/system/Language/en
/var/www/html/ci4/system/Language/sp (por ejemplo)

2.- En la carpeta  /var/www/html/ci4/system/Language/sp 
En el archivo Validation.php Traducir los mensajes que están en inglés

3.- en la carpeta /var/www/html/ci4/app/Config:
Cambiar en App.php la variable $defaultLocale = 'sp'
en la sección Default Local (línea 72 aprox.).
*/

/* WRITEPATH se configura en Config/Paths.php */

class Cformularioupload extends BaseController
{
	public function index()
	{
		/* Invoco la Librería para Validar Formularios */
		helper(['form']);
		$error=true;

		if ($this->request->getMethod() == 'post')
		{
			$fecha=date('Y-m-d');
			//die($fecha);
			/* Estas reglas de validación pueden definirse fuera del condicional */
			$validacion_archivo = 
			[
				/* Especificación simple de validaciones para un campo: */
				// 'correo' => 'required|valid_email',
				/* Especificación de validaciones para un campo en forma de Array: */
/*				'correo' =>
				[
					'rules' =>'required|valid_email',
					'label' =>'Dirección de Correo',
					'errors' =>
					[
						'required' => 'Atención, el Renglón Correo es Requerido',
						'valid_email'=>'Por favor, coloque un contenido válido para el correo',
					]
				],
				'clave'  => 'required|min_length[8]',
				'categoria' => 'required|in_list[estudiante,docente,director]',
				'fecha' =>
				[
					'rules' =>'required|mi_verificacion_de_fecha',
//					'rules' =>'required',
					'label' => 'fecha de algo',
					'errors' =>
					[
						'required' => 'Debes especificar una fecha de algo pana mio ... ',
						'mi_verificacion_de_fecha' => 'Especifique una fecha no menor a la actual',
					]
				],*/
				//1.- Crear una Carpeta (Validaciones, por ejemplo), dentro de app
				//2.- Dentro de esta carpeta crear una archivo (MisReglas.php, por ejemplo)
				//3.- Agregar en el archivo Validation.php Dentro de la Carpeta app/Config
				//    en el Set de Reglas personalizadas en el Array de $ruleSets al final, así:
				//    public $ruleSets = [
				// 	  ...
				// 	  \CodeIgniter\Validation\CreditCardRules::class,
				// 	  \App\Validaciones\MisReglas::class,
				'curriculum' =>
				[
					'rules' => 'uploaded[curriculum]|max_size[curriculum,100|ext_in[curriculum,pdf,doc,odt,docx]',
					'label' => 'El Curriculum',
				],
			];
			$validacion_imagen =
			[
				'foto' =>
				[
					'rules' => 'uploaded[foto]|max_size[foto,1000]|ext_in[foto,jpg,png,jpeg]',
					'label' => 'La fotografía'
				]
			];
			if($this->validate($validacion_archivo))
			{
				$curriculum = $this->request->getFile('curriculum');
				if($this->validate($validacion_imagen))
				{
					$photo = $this->request->getFile('foto');
					if($photo->isValid() && !$photo->hasMoved())
					{
						if($curriculum->isValid() && !$curriculum->hasMoved())
						{
							//$archivo_subido->move(WRITEPATH . 'carpeta/','nombreopcional'.$archivo_subido->getExtension(););
							$photo->move(WRITEPATH . 'imagenes/');
							$curriculum->move(WRITEPATH . 'documentos/');
						}
					}
					$nombreDoc=$curriculum->getName();
					$tipoDoc=$curriculum->getExtension();
					$rutaDoc=$curriculum->getTempName();

					$nombrePhoto=$photo->getName();
					$tipoPhoto=$photo->getExtension();
					$rutaPhoto=$photo->getTempName();

					//Datos del usuario que sube los archivos
	       			$session=session();
	       			$usuarioquesube=$_SESSION['nomb'];
	       			//print_r($_SESSION);die();

/*					echo( $nombreDoc . '<br>' . $tipoDoc . '<br>' . $rutaDoc . '<br>');
					echo( $nombrePhoto . '<br>' . $extPhoto . '<br>' . $rutaPhoto . '<br>');
					echo($usuarioquesube);*/

					//echo ($curriculum->getName() . '<br>' . $photo->getName());
					// Registro la información en la tabla (archivos_subidos) de ci4
					$db = \Config\Database::connect();

				    $strQuery="INSERT INTO archivos_subidos ";
				    $strQuery.="(";		
				    $strQuery.="nombre";
				    $strQuery.=",tipo";
				    $strQuery.=",ruta";
				    $strQuery.=",usuario";
				    $strQuery.=",fecha";				    
				    $strQuery.=")";
				    $strQuery.=" VALUES ";
				    $strQuery.="(";		
				    $strQuery.="'$nombreDoc'";
				    $strQuery.=",'$tipoDoc'";
				    $strQuery.=",'$rutaDoc'";
				    $strQuery.=",'$usuarioquesube'";
				    $strQuery.=",'$fecha'";				    
				    $strQuery.=")";
				    $strQuery.=",";
				    $strQuery.="(";		
				    $strQuery.="'$nombrePhoto'";
				    $strQuery.=",'$tipoPhoto'";
				    $strQuery.=",'$rutaPhoto'";
				    $strQuery.=",'$usuarioquesube'";
				    $strQuery.=",'$fecha'";				    
				    $strQuery.=");";
	       			//die($strQuery);
			        if($db->query($strQuery))
			        {
				    	$msg = 'La imagen y el archivo han sido registrados exitosamente';
				    	$tipo_mensaje="text-success";
					$error=false;
			        }
			        else
			        {
				    	$msg = 'Error al registrar documentos';
    					$tipo_mensaje="text-danger";
			        }
				}
				else
				{
					//echo ('La fotografía no es del tipo especificado, o excede el tamaño máximo permitido');
					$msg = 'La fotografía no es del tipo especificado, o excede el tamaño máximo permitido';
					$tipo_mensaje="text-danger";
				}
				//echo $file->getName();
				//exit;
				//return redirect()->to('/validarformulario/validacionexitosa');
				//echo $file->getName();
			}
			else
			{
				//echo ('El curriculum no es del tipo especificado, o excede el tamaño máximo permitido');
				$msg='El curriculum no es del tipo especificado, o excede el tamaño máximo permitido';
				$tipo_mensaje="text-info";
				//$data['validacion']=$this->validator;
			}
			//exit;
			////$data['validacion']=$this->validator;
			//$data['msg']=$msg;
			//$data['tipo_mensaje']=$tipo_mensaje;
			$data=
			[
			 'msg'     => $msg, 
			 'rutaimagen'    => '/uploads/imagenes/',
			 'rutaarchivo'    => '/uploads/documentos/',
			 'imagen' => $nombrePhoto,
			 'archivo' => $nombreDoc,
			 'error' => $error
			];
		
			return view('layout/header.php') .
			view('layout/menu.php') .
			view('estructura/uploads/subidadearchivosvalidada', $data) .
			view('estructura/footer_full.php');	

			//return redirect()->to("/validarformulario/validarsubidadearchivos");			
	   }
		/* Lleno el arreglo de las categorías */
		$data=[];
		$data['categorias']=
		[
			'estudiante',
			'docente',
			'director'
		];

		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/uploads/vformularioupload.php', $data) .
		view('estructura/footer_full.php');	
	}

	function validacionexitosa()
	{
		$data['resultado']='Exitosamente';
		
		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/validacion_de_formulario/formulario_validado', $data) .
		view('estructura/footer_full.php');	
	}

	//--------------------------------------------------------------------

}
