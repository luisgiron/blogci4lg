<?php namespace App\Controllers;

class Cupload extends BaseController
{
     function __construct()
     {
	   //helper('download');
     }

     public function index()
     {
	  $session=session();
	  //echo($_SESSION['nomb']);die();
	  $usuario=$_SESSION['nomb'];

	  $data['error'] = "";
	  $data['errorArch'] = "";
	  $data['estado'] = "";
	  $data['archivo'] = "";

	  $data['usuario'] = $usuario;

	  return
	  view('layout/header.php') .
	  view('layout/menu.php') .
	  view('estructura/uploads/vupload.php', $data) . 
	  view('estructura/footer_full.php');

     }
     public function subir_imagen()
     {
	  helper(['form', 'url']);

	  //Establesco las validaciones del archivo a subir
	  $validated = $this->validate
	  (
	       [
		    //Debo utilizar el mismo nombre que está en la Vista:
		    'imagen' => ['uploaded[imagen]'
			 ,'mime_in[imagen,image/jpg,image/jpeg,image/gif,image/png,document/pdf]'
			 ,'max_size[imagen,4096]',],
	       ]
	  );

	  /*
	  //Subo el archivo:
	  $imagen_subida = $this->request->getFile('imagen');
	  echo($imagen_subida)->getName();
	  echo('<br>');
	  echo($imagen_subida)->getExtension();//Esto solo se puede hacer con Move
	  echo('<br>');
	  //$tipo_imagen  = $imagen->getClientMimeType() O ESTO PARA TIPO ARCHIVO
	  echo($imagen_subida)->getTempName();
	  echo('<br>');
	  exit();
	  */
	  if ($validated)
	  {
	       $session=session();
	       $usuarioquesube=$_SESSION['nomb'];

	       /* Subo la imagen:  */
	       $imagen_subida = $this->request->getFile('imagen');		
	       //$avatar = $this->request->getFile('imagen')->store();
	       //WRITEPATH se configura en Config/Paths.php
	       $imagen_subida->move(WRITEPATH . 'imagenes/');
	       $nombre_imagen = $imagen_subida->getName(); //Esto solo se puede hacer con Move
	       $tipo_imagen  = $imagen_subida->getExtension();//Esto solo se puede hacer con Move
	       //$tipo_imagen  = $imagen->getClientMimeType() O ESTO PARA TIPO ARCHIVO
	       $ruta_imagen = $imagen_subida->getTempName();
	       /*
	       echo('Nombre=' . $nombre_imagen);
	       echo('<br>Extension=' . $tipo_imagen);
	       echo('<br>Ruta=' . $ruta_imagen);
	       die();
	       */

	       $db = \Config\Database::connect();
	       $strQuery="INSERT INTO archivos_subidos ";
	       $strQuery.="(";		
	       $strQuery.="nombre";
	       $strQuery.=",tipo";
	       $strQuery.=",ruta";
	       $strQuery.=",usuario";
	       $strQuery.=")";
	       $strQuery.=" VALUES ";
	       $strQuery.="(";		
	       $strQuery.="'$nombre_imagen'";
	       $strQuery.=",'$tipo_imagen'";
	       $strQuery.=",'$ruta_imagen'";
	       $strQuery.=",'$usuarioquesube'";
	       $strQuery.=");";
	       //die($strQuery);
	       if($db->query($strQuery))
	       {
		    $msg = 'La imagen ha sido subida y guardada';
	       }
	       else
	       {
		    $msg = 'Error al registrar imagen';
	       }
	  }
	  else
	  {
	       //die(' No Válido ... ');
	       throw new RuntimeException($file->getErrorString().'('.$file->getError().')');        	
	       $msg='Proceso de Carga de Imagen Fallido';
	  }

	  $data = [
	  'msg'     => $msg, 
	  'ruta'    => '/uploads/imagenes/',
	  'archivo' => $nombre_imagen
	  ];

	  return
	  view('layout/header.php') .
	  view('layout/menu.php') .
	  view('estructura/uploads/vimagensubida.php', $data) .
	  view('estructura/footer_full.php');
     }
}
