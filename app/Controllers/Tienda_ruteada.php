<?php namespace App\Controllers;

class Tienda_ruteada extends BaseController
{
     public function index()
     {
	  return 'Estoy en la Tienda';
     }
     public function producto($tipo_producto='Laptop', $identificador='Dell')
     {
	  return view('layout/header') .
	  view('layout/menu') .
	  "<h2>Estoy en el producto:$tipo_producto, con el identificador:$identificador</h2>" .
	  view('estructura/footer_full');
     }
     public function product()
     {
	  return "<div>" .
	  view('layout/header') .
	  view('layout/menu') .
	  'Respuesta del Método Invocado (Producto) configurado desde Routes...' .
	  view('estructura/footer_full') .
	  "</div>";
     }
}
