<?php namespace App\Controllers;

use App\Controllers\Estructura\Tienda As AdminTienda;

class Instanciar_clases extends BaseController
{
     public function index()
     {
	  return 'Estoy en Instanciar Clases ...';
     }
     public function instanciar_tiendas()
     {
	  $tienda = new Tienda();
	  echo $tienda->producto('Micro', 'Whirlpool') . '<br>';

	  $tiendaAdmin = new AdminTienda();
	  return $tiendaAdmin->producto();
     }
}
