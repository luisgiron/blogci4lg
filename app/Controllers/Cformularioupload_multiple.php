<?php namespace App\Controllers;

class Cformularioupload_multiple extends BaseController
{
	public function index()
	{
		/* Invoco la Librería para Validar Formularios */
		helper(['form']);
		$error=true;

		if ($this->request->getMethod() == 'post')
		{
			$fecha=date('Y-m-d');
			//die($fecha);
			/* Estas reglas de validación pueden definirse fuera del condicional */
			$validacion_de_archivos = 
			[
				'archivos' =>
				[
					'rules' => 'uploaded[archivos.0]|max_size[archivos,1000|ext_in[archivos,pdf,doc,odt,docx]',
					'rules' => 'uploaded[archivos.1]|max_size[archivos,1000|ext_in[archivos,pdf,doc,odt,docx]',					
					//'label' => 'Archivos Subidos',
				],
			];
			if($this->validate($validacion_de_archivos))
			{
				$archivos = $this->request->getFiles('archivos');
       			$session=session();
       			$usuarioquesube=$_SESSION['nomb'];

				foreach ($archivos['archivos'] as $archivo) 
				{
					if($archivo->isValid() && !$archivo->hasMoved())
					{
						//$archivo_subido->move(WRITEPATH . carpeta/','nombreopcional'.$archivo_subido->getExtension(););
						$archivo->move(WRITEPATH . 'documentos/multiple');
						$nombreArch=$archivo->getName();
						$tipoArch  =$archivo->getExtension();
						$rutaArch  =$archivo->getTempName();

						////echo($nombreArch . '-' . $tipoArch . '-' . $rutaArch . '<br>');

						$db = \Config\Database::connect();

					    $strQuery="INSERT INTO archivos_subidos ";
					    $strQuery.="(";		
					    $strQuery.="nombre";
					    $strQuery.=",tipo";
				    	$strQuery.=",ruta";
					    $strQuery.=",usuario";
					    $strQuery.=",fecha";				    
					    $strQuery.=")";
					    $strQuery.=" VALUES ";
					    $strQuery.="(";		
				    	$strQuery.="'$nombreArch'";
					    $strQuery.=",'$tipoArch'";
					    $strQuery.=",'$rutaArch'";
					    $strQuery.=",'$usuarioquesube'";
					    $strQuery.=",'$fecha'";				    
					    $strQuery.=")";
		       			//die($strQuery);
				        if($db->query($strQuery))
				        {
					    	$msg = 'Documentos Registrados';
    						$tipo_mensaje="text-success";					    	
							$error=false;
				        }
				        else
				        {
					    	$msg = 'Error al registrar documentos';
    						$tipo_mensaje="text-danger";
			        	}
					}
				}
/*					echo( $nombreDoc . '<br>' . $tipoDoc . '<br>' . $rutaDoc . '<br>');
					echo( $nombrePhoto . '<br>' . $extPhoto . '<br>' . $rutaPhoto . '<br>');
					echo($usuarioquesube);*/

					//echo ($curriculum->getName() . '<br>' . $photo->getName());
					// Registro la información en la tabla (archivos_subidos) de ci4
				//echo $file->getName();
				//exit;
				//return redirect()->to('/validarformulario/validacionexitosa');
				//echo $file->getName();
			}
			else
			{
				//echo ('El curriculum no es del tipo especificado, o excede el tamaño máximo permitido');
				$msg='El curriculum no es del tipo especificado, o excede el tamaño máximo permitido';
				$tipo_mensaje="text-info";
				//echo($msg);
				//$data['validacion']=$this->validator;
			}
			//echo($msg);
			//exit;
			////$data['validacion']=$this->validator;
			//$data['msg']=$msg;
			//$data['tipo_mensaje']=$tipo_mensaje;
			
			$data=
			[
			 'msg'          => $msg, 
			 'error'        => $error,
			 'tipo_mensaje' => $tipo_mensaje
			];
			
			return view('layout/header.php') .
			view('layout/menu.php') .
			view('estructura/uploads/subidadearchivosvalidada_multiple', $data) .
			view('estructura/footer_full.php');	

			//return redirect()->to("/validarformulario/validarsubidadearchivos");			
	   }
		/* Lleno el arreglo de las categorías */

		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/uploads/vformularioupload_multiple.php') .
		view('estructura/footer_full.php');	
	}
}