<?php namespace App\Controllers;

class Municipios extends BaseController
{

	public function listarMunicipiosPorEstado()
	{
		//echo("Estoy en Empleado");
		$db = \Config\Database::connect();
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();
		$idEstado=$request->getPostGet('txtIdeEstado');		


		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',municipio';
		$strQuery.=' FROM ';
		$strQuery.=' municipio';
		$strQuery.=" WHERE id_estado=$idEstado";
		$strQuery.=" AND NOT eliminado";				
		$strQuery.=' ORDER BY municipio;';

		//echo $strQuery;die();

		$query = $db->query($strQuery);
		$municipios = $query->getResultArray();
		//$empleados = $query->getResult();
		echo json_encode($municipios);
	}

	public function index()
	{
		//echo("Estoy en Municipio");
		$db = \Config\Database::connect();

		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',municipio';
		$strQuery.=' FROM ';
		$strQuery.=' municipio';
		$strQuery.=" WHERE NOT eliminado";		
		$strQuery.=' ORDER BY municipio;';

		//echo $strQuery;die();
		
		$query = $db->query($strQuery);
		$municipios = $query->getResultArray();
		//$empleados = $query->getResult();
		echo json_encode($municipios);
		
	}

	//--------------------------------------------------------------------
}
