<?php namespace App\Controllers;

class Evaluaciones extends BaseController
{
	public function index()
	{
		return view('layout/header.php') .
		view('layout/menu.php') .
		view('estructura/evaluaciones/evaluaciones') .
		view('estructura/footer_full.php');
	}

	public function listar()
	{
		//echo("Estoy en Empleado");
		$db = \Config\Database::connect();

		$strQuery='SELECT ';
		$strQuery.='emp.id';
		$strQuery.=',emp.nombre';
		$strQuery.=',COALESCE(eva.primertrimestre,0) AS primertrimestre';
		$strQuery.=',COALESCE(eva.segundotrimestre,0) AS segundotrimestre';
		$strQuery.=',COALESCE(eva.tercertrimestre,0) AS tercertrimestre';
		$strQuery.=',COALESCE(eva.cuartotrimestre,0) AS cuartotrimestre';
		$strQuery.=',COALESCE(eva.evaluacionfinal,0) AS evaluacionfinal';
		$strQuery.=' FROM ';
		$strQuery.=' empleado emp';
		$strQuery.=' LEFT JOIN evaluaciones eva ON emp.id=eva.idEmpleado';
		$strQuery.=" WHERE NOT emp.eliminado";		
		$strQuery.=' ORDER BY emp.id DESC;';

		//Esto también puede evaluarse como data en la respuesta del Ajax
		//echo $strQuery;die();

		$query = $db->query($strQuery);
		$evaluaciones = $query->getResultArray();
		//$evaluaciones = $query->getResult();
		echo json_encode($evaluaciones);
	}

	//--------------------------------------------------------------------

}
