<?php namespace App\Controllers;

use App\Models\UsuarioModel;

class Posts extends BaseController
{
     /*
     public function __construct()
     {
	   helper('form');
     }
      */

     public function index()
     {
	  $session=session();
	  //return('que te pasa calabaza ...');
	  //return $session->get('id');
	  //return $_SESSION['id'];
	  if (isset($_SESSION['id']))
	  {
	       //$usuarioModel = new UsuarioModel($db);
	       //Muéstrame los borrados también
	       //$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
	       ////$usuarios=$usuarioModel->paginate(8);
	       ////$paginas =$usuarioModel->pager;
	       // $usuarios=array('usuarios' => $usuarios,'paginas'=>$paginas);
	       //$usuarios=array('usuarios' => $usuarios);
	       return
	       view('estructura/header') .
	       view('estructura/menu') .
	       view('posts/listar_posts') . 
	       view('estructura/footer_full');
	  }
	  else
	  {
	       //return ' ... Debe autenticarse con un correo válido primero (ci4.com/login)  ... !!!';
	       return view('estructura/escris.php') . view('estructura/login.php');			
	  }
     }

     public function asociar_imagen($id=null, $titulo=null)
     {
	  $data=
	  [
	     'id'=>$id,
	     'titulo'=>$titulo,
	  ];
	  return
	  view('estructura/header') .
	  view('estructura/menu') .
	  view('posts/asociar_imagen', $data) . 
	  view('estructura/footer_full');
	  // $request = \Config\Services::request();
	  // $id      =$request->getPostGet('id');
	  // return $id;
     }

     public function ver_imagen_asociada($id=null, $titulo=null, $nombre_imagen=null)
     {
	  $data=
	  [
	     'id'=>$id,
	     'titulo'=>$titulo,
	     'nombre_imagen'=>$nombre_imagen,
	  ];
	  return
	  view('estructura/header') .
	  view('estructura/menu') .
	  view('posts/ver_imagen_asociada', $data) . 
	  view('estructura/footer_full');
	  // $request = \Config\Services::request();
	  // $id      =$request->getPostGet('id');
	  // return $id;
     }

     public function subir_imagen()
     {
	  $request = \Config\Services::request();
	  $id_post = $this->request->getGetPost('idPost');
	  /*
	  $imagen_subida = $this->request->getFile('imagenPost');
	  echo($imagen_subida)->getName();
	  echo('<br>');
	   */
	  //Establesco las validaciones del archivo a subir
	  helper(['form', 'url']);
	  $validated = $this->validate
	  (
	       [
		    //Debo utilizar el mismo nombre que está en la Vista:
		    'imagen' => 
		    ['uploaded[imagenPost]'
		    ,'mime_in[imagenPost,image/jpg,image/jpeg,image/gif,image/png,document/pdf]'
		    ,'max_size[imagenPost,4096]',
		    ],
	       ]
	  );
	  if ($validated)
	  {
	       /* Subo la imagen:  */
	       $imagen_subida = $this->request->getFile('imagenPost');		

	       //$avatar = $this->request->getFile('imagenPost')->store();
	       
	       //WRITEPATH se configura en Config/Paths.php
	       $imagen_subida->move(WRITEPATH . 'imagenes/');
	       $nombre_imagen = $imagen_subida->getName(); //Esto solo se puede hacer con Move
	       $tipo_imagen  = $imagen_subida->getExtension();//Esto solo se puede hacer con Move
	       //$tipo_imagen  = $imagen->getClientMimeType() O ESTO PARA TIPO ARCHIVO
	       $ruta_imagen = $imagen_subida->getTempName();

	       /*
	       echo('Nombre=' . $nombre_imagen);
	       echo('<br>Extension=' . $tipo_imagen);
	       echo('<br>Ruta=' . $ruta_imagen);
	       */

	       $db = \Config\Database::connect();
	       //Eliminó la imagen anterior asociada al Post
	       $strQuery="UPDATE post_imagen ";
	       $strQuery.="SET activo='0' ";
	       $strQuery.="WHERE id_post=$id_post";
	       //Verifico la actualización
	       if($db->query($strQuery))
	       {
		    $strQuery="INSERT INTO post_imagen ";
		    $strQuery.="(";		
		    $strQuery.="id_post";
		    $strQuery.=",nombre_imagen";
		    $strQuery.=",tipo_imagen";
		    $strQuery.=",ruta_imagen";
		    $strQuery.=",activo";
		    $strQuery.=")";
		    $strQuery.=" VALUES ";
		    $strQuery.="(";		
		    $strQuery.="'$id_post'";
		    $strQuery.=",'$nombre_imagen'";
		    $strQuery.=",'$tipo_imagen'";
		    $strQuery.=",'$ruta_imagen'";
		    $strQuery.=",'1'";
		    $strQuery.=");";
		    if($db->query($strQuery))
		    {
			 $msg = 'La imagen se ha subido y guardado en BD';
		    }
		    else
		    {
			 $msg = 'Error al registrar la imagen en la BD';
		    }
	       }
	       else
	       {
		    $msg = 'Error en la actualización del histórico de imágenes';
	       }

	  }
	  else
	  {
	       $msg='No subió la imagen, favor verificar ...';
	  }
	  $data=
	  [
	       'mensaje'=>$msg
	  ];
	  return
	  view('estructura/header') .
	  view('estructura/menu') .
	  view('posts/mensajes', $data) . 
	  view('estructura/footer_full');
     }

	     public function listar()
	     {
		  //Invocado por ajax de posts.js para
		  //Llenar el Datatable de listar_posts;
		     $db = \Config\Database::connect();

		     $strQuery='SELECT ';
		     $strQuery.='id';
		     $strQuery.=',titulo';
		     $strQuery.=',autor';
		     $strQuery.=',fecha_publicacion';
		     $strQuery.=' FROM ';
		     $strQuery.=' posts';
		     $strQuery.=" WHERE activo=1";		
		     $strQuery.=' ORDER BY id DESC;';

		     ////echo json_encode($strQuery);
		     $query = $db->query($strQuery);
		     //$posts = $query->getResultArray();
		     $posts = $query->getResult();
		     echo json_encode($posts);
	     }

	     public function agregar()
	     {
		$request = \Config\Services::request();

		//Verifico si me llegó el dato (ojo falta agregar los demás)
		if ($request->getPostGet('titulo')&&$request->getPostGet('contenido')&&$request->getPostGet('autor'))
		{
		   $titulo      =$request->getPostGet('titulo');
		   $contenido   =$request->getPostGet('contenido');
		   $autor       =$request->getPostGet('autor');

		   $db = \Config\Database::connect();

		   $strQuery="INSERT INTO ";
		   $strQuery.="posts (";
		   $strQuery.="titulo";
		   $strQuery.=",contenido";
		   $strQuery.=",autor";
		   $strQuery.=") VALUES (";
		   $strQuery.="'$titulo'";
		   $strQuery.=",'$contenido'";
		   $strQuery.=",'$autor'";
		   $strQuery.=");";

		   ///Para debuguear el Query:
		   ///$mensaje=$strQuery;
		   ///$mensaje=$id_municipio;

		   //$db->query($strQuery);
		   //$resultado=$db->affectedRows();		
		   //
		    $mensaje['tipo']='incorporacion';
		    $mensaje['grabado']=false;

		   if($db->query($strQuery))
		   {
			 $mensaje['grabado']=true;
		   }
		   else
		   {
		      $mensaje['grabado']=false;
	      }
	   }
	   return json_encode($mensaje);
	}

	public function buscar_imagen_asociada()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();
		if ($request->getPostGet('id'))
		{
			$db = \Config\Database::connect();

			$id=$request->getPostGet('id');

			$strQuery="SELECT ";
			$strQuery.="id";
			$strQuery.=",id_post";
			$strQuery.=",nombre_imagen";
			$strQuery.=",ruta_imagen";
			$strQuery.=",tipo_imagen";
			$strQuery.=",activo";
			$strQuery.=" FROM ";
			$strQuery.=" post_imagen";
			$strQuery.=" WHERE id_post=$id";
			$strQuery.=" AND activo='1';";

			//Para debuguear el Query:
			//$mensaje=$strQuery;

			$resultado=$db->query($strQuery);
			$resultado=$resultado->getRowArray();			

			//$query = $db->query($strQuery);
			//$cedula = $query->getRowArray();
			//echo $nombre['nombre'];
			$mensaje=$resultado;
			/*
			if (isset($resultado))
			{
			     $mensaje='Tiene registros asociados';
			}
			else
			{
			     $mensaje='No Tiene registros asociados';
			}
			 */
		}
		else
		{
			$mensaje='No ha llegado Aide';
		}
		return json_encode($mensaje);		
	}

	public function buscar()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();
		if ($request->getPostGet('id'))
		{
			//$mensaje='Llego Aide';
			$db = \Config\Database::connect();

			$id=$request->getPostGet('id');

			$strQuery="SELECT ";
			$strQuery.="id";
			$strQuery.=",titulo";
			$strQuery.=",contenido";
			$strQuery.=",autor";
			$strQuery.=",fecha_publicacion";
			$strQuery.=" FROM ";
			$strQuery.=" posts";
			$strQuery.=" WHERE id=$id;";

			//Para debuguear el Query:
			////$mensaje=$strQuery;

			$resultado=$db->query($strQuery);
			$resultado=$resultado->getRowArray();			

			//$query = $db->query($strQuery);
			//$cedula = $query->getRowArray();
			//echo $nombre['nombre'];
			$mensaje=$resultado;
		}
		else
		{
			$mensaje='No ha llegado Aide';
		}

		return json_encode($mensaje);		
	}

	public function actualizar()
	{
	     //Libreria para halar los datos vía Get o Post
	     //o Vía Ajax, que es el caso  a través de la vaeiable data
		$request = \Config\Services::request();

		if ($request->getPostGet('id'))
		{
			$db = \Config\Database::connect();

			$mensaje['tipo']='actualizacion';
			$mensaje['grabado']=false;

			$id          =$request->getPostGet('id');
			$titulo      =$request->getPostGet('titulo');
			$contenido   =$request->getPostGet('contenido');
			$autor       =$request->getPostGet('autor');

			$strQuery="UPDATE  ";
			$strQuery.="posts ";
			$strQuery.="SET ";
			$strQuery.="titulo='$titulo'";
			$strQuery.=",contenido='$contenido' ";
			$strQuery.=",autor='$autor' ";
			$strQuery.="WHERE id=$id;";

			if($db->query($strQuery))
			{
				$mensaje['grabado']=true;
			}
			else
			{
				$mensaje['grabado']=false;
			}
		}
		else
		{
			$mensaje['grabado']=false;			
		}

		//Para debuguear el Query:
		////$msjDebug=$strQuery;
		////return json_encode($msjDebug);
		//return ($msjDebug);
							
		return json_encode($mensaje);
	}

	public function eliminar()
	{
		//Libreria para halar los datos vía Get o Post
		//O Vía Ajax, que es el caso
		$request = \Config\Services::request();

		if ($request->getPostGet('id'))
		{
			$db = \Config\Database::connect();

			$mensaje=false;

			$id       =$request->getPostGet('id');
			//$fecha_actualizacion=date('Y-m-d H:i:s');

			$strQuery="UPDATE  ";
			$strQuery.="posts ";
			$strQuery.="SET ";
			$strQuery.="activo=0 ";
			//$strQuery.=",actualizado_en='$fecha_actualizacion' ";
			$strQuery.="WHERE id=$id;";	

			if($db->query($strQuery))
			{
				$mensaje=true;
			}
			else
			{
				$mensaje=false;
			}
		}
		else
		{
			$mensaje=false;			
		}

		//Descomentar para debuguear el Query:
		//$mensaje=$strQuery;		
		//return json_encode($mensaje);
		//$mensaje="A donde ira";
		return json_encode($mensaje);
	}

	/* Borrame, por favor
	public function agregar()
	{
		return view('estructura/head.php') . view('estructura/usuario_agregar.php') . view('estructura/escris.php') . view('estructura/footer.php');
	}
	 */	
	public function guardar()
	{
		$usuarioModel =  new UsuarioModel($db);

		$request = \Config\Services::request();

		$datos = 
		[
			'id'=>$request->getPostGet('id'),
			'nombre'=>$request->getPostGet('nombre'),
			'correo'=>$request->getPostGet('correo')
		];
		//print_r($datos);die();
		//Verificamos que se guarde el Registro
		if($usuarioModel->save($datos)===false)
		{
			$dataErrores=['errores' =>$usuarioModel->errors()];
			//var_dump($dataErrores);
			//Muestro los mensajes configurados para cada error en una Vista:
			return view('estructura/actualizarUsuario',$dataErrores);
		}	

/*		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);	*/	
		return redirect()->to('/usuario');
	}
	public function editar()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();
		
		////$id=$request->getPostGet('id');
		//$id=$request->uri->getSegment(4);  //???
		//echo $request->uri->getSegment(4) . ' y ' . $request->uri->getSegment(5);
		//echo $request->uri->getTotalSegments();
/*		$segmentos=$request->uri->getSegments();
		var_dump($segmentos);
		die();*/

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);			
		}

		//var_dump($id);die();
		$usuario=$usuarioModel->find($id);
		$usuario=array('usuario' => $usuario);
		return view('estructura/head').view('estructura/usuario_agregar',$usuario) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}
	/*
	public function eliminar()
	{
		$usuarioModel = new UsuarioModel($db);
		$request = \Config\Services::request();

		if ($id=$request->getPostGet('id'))
		{
			$id=$request->getPostGet('id');
		}
		else
		{
			$id=$request->uri->getSegment(4);  //???			
		}
		//var_dump($id);die();		
		$usuarioModel->delete($id);

		$usuarios=$usuarioModel->orderBy('id','desc')->withDeleted()->findAll();
		$usuarios=array('usuarios' => $usuarios);
		//var_dump($usuarioEncontrado);
		
		return view('estructura/head.php') . view('estructura/usuario_listar.php', $usuarios) . view('estructura/escris.php') . view('estructura/usuario_datatable.php') . view('estructura/footer.php');
	}
	 */

	public function buscarlogin()
	{
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();
		if ($request->getPostGet('login'))
		{
			//$mensaje='Llego Aide';
			$db = \Config\Database::connect();

			$login=$request->getPostGet('login');

			$strQuery="SELECT ";
			$strQuery.="id";
			$strQuery.=",nombre";
			$strQuery.=" FROM ";
			$strQuery.=" usuario";
			$strQuery.=" WHERE correo='$login';";

			$resultado=$db->query($strQuery);
			$resultado=$resultado->getRowArray();

			//Para debuguear el Query:
			//$mensaje=$strQuery;
			
			//1.- Evaluo si la consulta trajo algo:			
			if (isset($resultado))
			{
				//Sin Montar nada en Sesion
				//$id    =$resultado['id'];
				//$nombre=$resultado['nombre'];

				//2.- Montar en sesión: id y nombre
				$session = session();							
				
				//2.a.- Se Montan las variables una por una en Sesión
				//$session->set('some_name', 'some_value');

/*				$session->set('id',$resultado['id']);
				$session->set('nomb',$resultado['nombre']);*/
				
				$id=$session->get('id');
				$nombre=$session->get('nomb');
				
				//2.b.- Se Montan las variables en un array
				$s_data=
				[
					'id'    =>$resultado['id'],
					'nomb'  =>$resultado['nombre']
				];
				$session->set($s_data);
				$id=$_SESSION['id'];

				$mensaje=1;
			}
			else
			{
				$mensaje=0;				
			}
			//3.- Ponerle un estilo a la vista
			//Mostrar los valores montados en sesión

			//$mensaje=$resultado;
			return json_encode($mensaje);
		}
		else
		{
			$mensaje='No ha llegado Aide';
			////return json_encode($mensaje);			
		}

		return json_encode($mensaje);		
	}	

	//--------------------------------------------------------------------

}
