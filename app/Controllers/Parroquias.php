<?php namespace App\Controllers;

class Parroquias extends BaseController
{

	public function listarParroquiasPorMunicipio()
	{
		$db = \Config\Database::connect();
		//Libreria para halar los datos vía Get o Post
		$request = \Config\Services::request();
		$idMunicipio=$request->getPostGet('txtIdMunicipio');		


		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',parroquia';
		$strQuery.=' FROM ';
		$strQuery.=' parroquia';
		$strQuery.=" WHERE id_municipio=$idMunicipio";
		$strQuery.=" AND NOT eliminado";				
		$strQuery.=' ORDER BY parroquia;';

		//echo $strQuery;die();

		$query = $db->query($strQuery);
		$parroquias = $query->getResultArray();
		//$empleados = $query->getResult();
		echo json_encode($parroquias);
	}

	public function index()
	{
		//echo("Estoy en Parroquia");
		$db = \Config\Database::connect();

		$strQuery='SELECT ';
		$strQuery.='id';
		$strQuery.=',parroquia';
		$strQuery.=' FROM ';
		$strQuery.=' parroquia';
		$strQuery.=" WHERE NOT eliminado";		
		$strQuery.=' ORDER BY parroquia;';

		//echo $strQuery;die();
		
		$query = $db->query($strQuery);
		$parroquias = $query->getResultArray();
		//$empleados = $query->getResult();
		echo json_encode($parroquias);
		
	}

	//--------------------------------------------------------------------
}
