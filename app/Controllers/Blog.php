<?php namespace App\Controllers;

class Blog extends BaseController
{
	public function index()
	{
		$valores=
		[
		     'Valor uno del array',
		     'Valor dos del array',
		     'Valor tres del array',
		     'Valor cuatro del array',
		     'Valor cinco del array',
		];

		$data = 
		[
			'meta_title' => 'Codeigniter 4 Blog',
			'title' => 'Este es mi blog',
			'valores' => $valores
		];
		return
	  	view('layout/header.php') .
	  	view('layout/menu.php') .

	  	view('blog_header') .
		view('blog', $data) .
		view('blog_footer') .

	  	view('estructura/footer_full.php');
	}
	public function post()
	{
		$data = 
		[
			'meta_title' => 'Codeigniter 4 Blog',
			'title' => 'Esta es la Página Single Post de mi blog'
		];		
		return
	  	view('layout/header.php') .
	  	view('layout/menu.php') .

	  	view('blog_header') .
		view('single_post', $data) .
		view('blog_footer') .

	  	view('estructura/footer_full.php');
		//return view('single_post');
	}
}
