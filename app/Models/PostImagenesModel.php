<?php namespace App\Models;

use CodeIgniter\Model;

class PostImagenesModel extends Model
{
     protected $table = 'post_imagen';
     protected $primaryKey = 'id';
}
