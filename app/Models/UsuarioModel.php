<?php namespace App\Models;

use CodeIgniter\Model;

class UsuarioModel extends Model
{
        protected $table      = 'usuario';
        protected $primaryKey = 'id';

        protected $returnType = 'array';
        protected $useSoftDeletes = true;

        protected $allowedFields = ['nombre', 'correo', 'clave'];
/*
        protected $useTimestamps = false;
        protected $createdField  = 'creado_en';
        protected $updatedField  = 'actualizado_en';
        protected $deletedField  = 'eliminado';

        protected $validationRules    = 
        [
        	'nombre'=>'required|alpha_numeric_space|min_length[10]',
        	// 'correo'=>'required|valid_email|is_unique[usuario.correo]'
        	'correo'=>'required|valid_email'
        ];
        protected $validationMessages =
        [
        	'nombre'=>
        	[
        		'required'=>'Debe proporcionar un nombre',
        		'alpha_numeric_space'=>'El campo nombre Debe ser alpha numérico con espacios',
        		'min_length'=>'El tamaño mínimo del nombre debe ser de 10 caracteres'
        	],
        	'correo'=>
        	[
        		'required'=>'Debe Proporcionar un correo',
        		'valid_email'=>'El Correo Debe tener un Formato Válido'
        		// 'is_unique'=>'Lo siento este correo ya está registrado'
        	]
        ];
        protected $skipValidation     = false;
*/
//TRIGGERS:
/*        protected $beforeInsert=['agregarAlgoAlNombre'];
        protected $beforeUpdate=['agregarAlgoAlNombre'];

        protected function agregarAlgoAlNombre(array $data)
        {
        	$data['data']['nombre']=$data['data']['nombre'] . " Agregado";
        	return $data;
        }*/
}
