<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

class CustomModel
{
     protected $db;

     public function __construct(ConnectionInterface &$db)
     {
	$this->db =& $db;
     }	
     function all()
     {
	  return $this->db->table('posts')
	       ->get()
	       ->getResult();
     }
     function where()
     {
	  return $this->db->table('posts')
	       ->where(['post_id >=' => 1])
	       ->where(['post_id <=' => 2])
	       ->get()
	       ->getResult();
	       //->getRow();
     }
     function join()
     {
	  return $this->db->table('posts')
	       ->where(['post_id >=' => 1])
	       ->where(['post_id <=' => 2])
	       ->join('blog_users', 'posts.post_author = blog_users.id')
	       ->get()
	       ->getResult();
	       //->getRow();
     }
     function like()
     {
	  return $this->db->table('posts')
	       ->like('post_content', 'timo')                //Default %string% ('both')
	       //->like('post_id', 'timo', 'before')   //%strin%
	       //->like('post_id', 'timo', 'after)     //strin%
	       ->join('blog_users', 'posts.post_author = blog_users.id')
	       ->get()
	       ->getResult();
	       //->getRow();
     }
     function getPosts()
     {
	  $builder = $this->db->table('posts');
	  $builder->join('blog_users','posts.post_author=blog_users.id');
	  $posts = $builder->getResult();
	  return $posts;
     }
}
