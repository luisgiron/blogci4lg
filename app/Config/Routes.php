<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
//$routes->get('/adminblog', 'Posts::index');
$routes->get('/', 'Home::index');
$routes->get('/adminblog','Home::admininicio');
$routes->post('/adminblog','Home::admininicio');
$routes->get('/entradas', 'Posts::index');
$routes->post('/entradas/agregar', 'Posts::agregar');
$routes->get('/entradas/buscar', 'Posts::buscar');
$routes->post('/entradas/actualizar', 'Posts::actualizar');
$routes->post('/entradas/eliminar', 'Posts::eliminar');
$routes->get('/entradas/asociar_imagen/(:any)/(:any)','Posts::asociar_imagen/$1/$2');
$routes->post('/entradas/subir_imagen','Posts::subir_imagen');
$routes->get('/entradas/ver_imagen_asociada/(:any)/(:any)/(:any)','Posts::ver_imagen_asociada/$1/$2/$3');
$routes->get('/entradas/buscar_imagen_asociada', 'Posts::buscar_imagen_asociada');
$routes->get('/registrouser','Home::registrousuario');
//$routes->post('/usuario/buscarusuario','Usuario::buscar_login');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
/* Para crear Grupo de Rutas, en este caso dentro de la carpeta Admin */
/* Igual en la URL hay que anteponer en el primer segmento el nombre del grupo creado:
 * esto es, para este caso: dominio_creado.com/admin/user o
 * dominio_creado.com/users_muestralos */
