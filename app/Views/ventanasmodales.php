<br>
<div class="row">
     <div class="col-md-4">
	  <div>
	       <h1>Ventanas Modales</h1>
	  </div>
     </div>
</div>
<hr>
<div class="row">
     <div class="col-md-4">
	  <div class="alert alert-info">
	       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#miVentanaModal">
		    Demo de Ventanas Modales
	       </button>
	  </div>
     </div>
</div>


<!-- Modal -->
<div class="modal fade" id="miVentanaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Este es el titulo de la Ventana Modal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Este es el body de la ventana modal</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>
