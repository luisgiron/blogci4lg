<div class="container">
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <h1><?= (isset($title)? $title: 'Falta especificar el valor del Título'); ?></h1>
	  </div>
     </div>
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <form method="post" action="/blog_Suelto/new">
		    <div class="form-group">
			 <label for="post_title">Título</label>
			 <input id="" class="form-control" type="text" name="post_title">
		    </div>
		    <div class="form-group">
			 <label for="post_title">Texto</label>
			 <textarea id="" class="form-control" name="post_content" rows="3"></textarea>
		    </div>
		    <div class="form-group">
			 <button class="btn btn-success">Agregar</button>
		    </div>
	       </form>
	  </div>
     </div>
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <a href="/blog_Suelto/" class="btn btn-secondary">Ver Todos los Títulos</a>
	  </div>
     </div>
</div>
