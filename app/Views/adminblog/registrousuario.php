<!--body class="hold-transition register-page"-->
<body class="hold-transition login-page">
<!--div class="register-box"-->
<div class="login-box">
  <!--div class="register-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div-->
  <div class="login-logo">
    <p><b>Registrar un Usuario Nuevo</a></p>
  </div>
  <!-- /.login-logo -->

  <div class="card">
    <!--div class="card-body register-card-body"-->
    <div class="card-body login-card-body">
      <p class="login-box-msg">Por favor, suministre los datos<br>Solicitados a continuación</p>

      <!--form action="<?= base_url(); ?>/usuario/guardar" id="formUsuario" method="post"-->
      <form action="" id="formUsuario" method="post">
        <div class="input-group mb-4">
          <input type="text" class="form-control" placeholder="login" id="idlogin" name="login" data-toggle="tooltip" title="Por favor, suministre su Nombre de Usuario">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-4">
          <input type="email" class="form-control" placeholder="sucorreo@dominio.extension" id="idcorreo" name="correo" data-toggle="tooltip" title="Por favor suministre su correo, no olvide colocar el @nombrededominio y su extensión separada por un punto">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-1">
	  <label>Su clave debe contener al menos Seis (6) Caracteres:</label>
	</div>
        <div class="input-group mb-4">
          <input type="password" class="form-control" placeholder="Clave" id="idclave" name="clave" data-toggle="tooltip" title="Por favor suministre una clave de acceso de AL MENOS 6 CARACTERES">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-4">
          <input type="password" class="form-control" placeholder="Reescriba su clave" id="idclaveotravez" name="claveotravez" data-toggle="tooltip" title="Por favor, repita el valor de la clave suministrada en el campo anterior">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <!--div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div-->
          <!-- /.col -->
          <!--div class="col-4"-->
          <div class="col-12">
            <!--button type="submit" class="btn btn-success btn-block">Registrar</button-->
            <button class="btn btn-success btn-block" id="btnRegistrar" name="btnRegistrar">Registro</button>
            <button class="btn btn-warning btn-block" id="btnCancelarRegistro" name="btnCancelarRegistro">Cancelar Registro</button>
          </div>
          <!-- /.col -->
	</div>
     <div class="alert alert-warning" style="display: none;"></div>

      </form>

      <!--div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i>
          Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div-->

      <!--a href="login.html" class="text-center">I already have a membership</a-->
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<script type="text/javascript">
     var base_url="<?= base_url(); ?>"
</script>
<!-- /.register-box -->

<!-- jQuery -->
<!--script src="../../plugins/jquery/jquery.min.js"></script-->

<script src="<?=base_url(); ?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<!--script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script-->
<!--script src="<?=base_url(); ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script-->
<!-- AdminLTE App -->
<!--script src="../../dist/js/adminlte.min.js"></script-->
<script src="<?=base_url(); ?>/assets/dist/js/adminlte.min.js"></script>
</body>
</html>

<script type="text/javascript">
$('#btnCancelarRegistro').click(function()
{
     event.preventDefault();
     alert('Cancelando Registro de Usuario');
     //alert(base_url);
     $(location).attr('href',base_url+'/adminblog');
});
$('#btnRegistrar').click(function()
{
     event.preventDefault();
     var login        ='';
     var correo       ='';
     var correovalido ='';
     var clave        ='';
     var clavesiguales='';
     var longitudclave='';

     login=verificarlogin();
     if(login==0)
     {
	  //alert('Es cero');
	  $('.alert-warning').html('Debe suministrar un nombre/login').fadeIn().delay(3000).fadeOut('slow');
     }
     else
     {
	  //alert('No es cero');
	  correo=verificarcorreo();
	  if(correo==0)
	  {
	       //alert('Es cero');
	       $('.alert-warning').html('Debe suministrar un correo/login').fadeIn().delay(3000).fadeOut('slow');
	  }
	  else
	  {
	       //alert('No es cero');
	       correovalido=verificarcorreovalido();
	       if(correovalido==0)
	       {
		    //alert('Es cero');
		    $('.alert-warning').html('La direccón de correo no tiene buen contenido').fadeIn().delay(3000).fadeOut('slow');
	       }
	       else
	       {
		    //alert('No es cero');
		    clave=verificarclave();
		    if(clave==0)
		    {
			 //alert('Es cero');
			 $('.alert-warning').html('Debe suministrar una clave').fadeIn().delay(3000).fadeOut('slow');
		    }
		    else
		    {
			 //alert('No es cero');
			 clavesiguales=verificarClavesIguales();
			 alert(clavesiguales);
			 if(clavesiguales==0)
			 {
			      //alert('Es cero');
			      $('.alert-warning').html('Las Claves deben ser iguales').fadeIn().delay(3000).fadeOut('slow');
			      $('#idclaveiotravez').val('');
			      $('#idclaveotravez').focus();
			 }
			 else
			 {
			      //alert('No es cero');
			      verificarUsuario();
			 }
		    }
	       }
	  }
     }
});

function verificarlogin()
{
     if($('#idlogin').val()==='')
     {
	  return 0;
     }
     else
     {
	  return 1;
     }
}
function verificarcorreo()
{
     if($('#idcorreo').val()==='')
     {
	  return 0;
     }
     else
     {
	  return 1;
     }
}
function verificarcorreovalido()
{
     var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

     if (regex.test($('#idcorreo').val().trim()))
     {
	  return 1;
     }
     else
     {
	  return 0;
     }
}
function verificarclave()
{
     if($('#idclave').val()=='')
     {
	  return 0;
     }
     else
     {
	  return 1;
     }
}
$('#idclaveotravez').click(function()
{
     var tamanioclave =6;
     alert('Verificando tamaño de clave');
     longitudclave=verificarLongitudClave(tamanioclave);
     if(longitudclave==0)
     {
	  $('.alert-warning').html('La clave suministrada debe ser de, al menos, ' + tamanioclave +' caracteres').fadeIn().delay(3000).fadeOut('slow');
	  $('#idclave').val('');
	  $('#idclave').focus();
     }
});
function verificarLongitudClave(tamanioclave)
{
     event.preventDefault();
     alert('Estoy en verificar clave');
     //alert(tamanioclave);
     var clave;
     clave=$('#idclave').val();
     alert(clave);
     alert(clave.length);
     if(clave.length<6)
     {
	  return 0;
     }
     else
     {
	  return 1;
     }
}
function verificarClavesIguales()
{
     event.preventDefault();
     /*
     alert($('#idlogin').val());
     alert($('#idcorreo').val());
     alert($('#idclave').val());
     alert($('#idclaveotravez').val());
      */
     if($('#idclave').val()===$('#idclaveotravez').val())
     {
	  return 1;
     }
     else
     {
	  return 0;
     }
}
function verificarUsuario()
{
     var base="<?= base_url(); ?>/usuario/buscar_login";
     //alert(base_url);
     var login = $('#idlogin').val();  
     var ruta=base_url;
     //alert(login);
     //$.support.cors=true;
     $.ajax(
     {
	  url:base,
	  type:'ajax',
	  method:'post',
	  data:
	  {
	       login:login
	  },
	  dataType:'json',
	  beforeSend:function()
	  {
	       alert(' ... procesando la  petición, por favor espere un momento ...');
	  },
	  success:function(data)
	  {
	       alert(data);        
	       if(data===1)
		    alert('... Este usuario ya se encuentra Registrado, por favor verifique ...');
	       else
	       {
		    alert('No Existe');
		    ingresarUsuario();
		    //$('#formUsuario').attr('action',<?= base_url(); ?>+'/usuario/guardar');
		    //$(location).attr('href',ruta+'/usuario/guardar');
	       }
	  },
	  error:function(xhr, status, errorThrown)
	  {
	       alert('Eeeerrror ...');
	       alert(xhr.status);
	       alert(errorThrown);
	  }
     });
}
function ingresarUsuario()
{
     var base="<?= base_url(); ?>/usuario/guardar";
     var login =$('#idlogin').val();
     var correo=$('#idcorreo').val();
     var clave =$('#idclave').val();
     var clave2=$('#idclaveotravez').val();
     var data=
     {
	  "nombre":login,
	  "correo":correo,
	  "clave":clave
     }
     /*
     alert(login);
     alert(correo);
     alert(clave);
     alert(clave2);
      */
     $.ajax(
     {
	  url:base,
	  type:'ajax',
	  method:'post',
	  data:data,
	  dataType:'json',
	  beforeSend:function()
	  {
	       alert(' ... procesando la  petición, por favor espere un momento ...');
	  },
	  success:function(data)
	  {
	       if(data===1)
	       {
		    alert(data);
		    alert(' ... al parecer se procesó ...');
		    $(location).attr('href',base_url+'/adminblog');
	       }
	       else
	       {
		    alert(' Usuario no registrado, por favor verifique ...');
	       }
	  },
	  error:function(xhr, status, errorThrown)
	  {
	       alert('Eeeerrror ...');
	       alert(xhr.status);
	       alert(errorThrown);
	  }
     });
}
</script>
