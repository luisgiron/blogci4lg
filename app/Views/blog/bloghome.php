<!-- Page Content -->
<div class="container">

<!-- Este es el Carrusel -->
<div class="row">
	<div class="col-md-12">

		<div id="carouselLG" class="carousel slide" data-ride="carousel">
		  <div class="carousel-inner">

		    <div class="carousel-item active">
		      <img class="d-block w-100" src="" alt="GALERÍA DE IMAGENES BLOG LG">
		    </div>

		    <?php foreach($posts as $post): ?>
			<?php foreach($imagenes as $imagen): ?>
				<?php if($imagen['id_post']==$post['id']): ?>
				    <div class="carousel-item">
				      <img class="d-block w-100" src="<?= base_url(); ?>/uploads/imagenes/<?= $imagen['nombre_imagen']; ?>" alt="Second slide">
					<div class="carousel-caption d-none d-md-block">
						<h5><strong><?= $post['titulo']; ?></strong></h5>
						<p><h3><?= $post['contenido']; ?></h3></p>
					</div>
				    </div>
				<?php endif ?>
			<?php endforeach ?>
		    <?php endforeach ?>

		  </div>
		<a class="carousel-control-prev" href="#carouselLG" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previo</span>
		</a>
		<a class="carousel-control-next" href="#carouselLG" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Siguiente</span>
		</a>
		</div>
	</div>
</div>

<div class="row">
     <!-- Blog Entries Column -->
     <div class="col-md-8">
	  <div>
	       <h1 class="my-4"><strong>Posts<small> más recientes</small></strong></h1>
	  </div>
     <!-- Blog Post -->
     <?php foreach($posts as $post): ?>
     <div class="card mb-4">
	  <?php foreach($imagenes as $imagen): ?>
	       <?php if($imagen['id_post']==$post['id']): ?>
	       <img class="card-img-top" src="uploads/imagenes/<?= $imagen['nombre_imagen']; ?>" alt="Card image cap">
	       <?php endif ?>
	  <?php endforeach; ?>
	  <div class="card-body">
	       <h2 class="card-title"><?= $post['titulo']; ?></h2>
	       <p class="card-text">
		    <?= $post['contenido']; ?>
	       </p>
	       <!--a href="#" class="btn btn-primary">Read More &rarr;</a-->
          </div>
          <div class="card-footer text-muted">
	       <p>
		    Autor: <?= $post['autor']; ?>
	       </p>
            <!--a href="#">Start Bootstrap</a-->
	  </div>
     </div>
     <?php endforeach; ?>

     </div>
     <!-- /.row -->
</div>
<!-- /.container -->
