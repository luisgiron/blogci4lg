<!-- Page Content -->
<div class="container">

<!-- Este es el Carrusel -->
	<div class="row">
		<div class="col-md-12">
			<div class="carousel slide" id="carousel-515299">
				<ol class="carousel-indicators">
					<li data-slide-to="0" data-target="#carousel-515299">
					</li>
					<li data-slide-to="1" data-target="#carousel-515299" class="active">
					</li>
					<li data-slide-to="2" data-target="#carousel-515299">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item">
						<!--img class="d-block w-100" alt="Carousel Bootstrap First" src="https://www.layoutit.com/img/sports-q-c-1600-500-1.jpg"-->
						<img class="d-block w-100" alt="Carousel Bootstrap First" src="<?= base_url(); ?>/imagenes/surf.jpg">
						<div class="carousel-caption">
							<h4>
								Tercer Post Relevante
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="carousel-item active">
						<!--img class="d-block w-100" alt="Carousel Bootstrap Second" src="https://www.layoutit.com/img/sports-q-c-1600-500-2.jpg"-->
						<img class="d-block w-100" alt="Carousel Bootstrap First" src="<?= base_url(); ?>/imagenes/bateador.jpg">
						<div class="carousel-caption">
							<h4>
								Primer Post Relevante
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="carousel-item">
						<!--img class="d-block w-100" alt="Carousel Bootstrap Third" src="https://www.layoutit.com/img/sports-q-c-1600-500-3.jpg"-->
						<img class="d-block w-100" alt="Carousel Bootstrap First" src="<?= base_url(); ?>/imagenes/ciclista.jpg">
						<div class="carousel-caption">
							<h4>
								Segundo Post Relevante
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
				</div> <a class="carousel-control-prev" href="#carousel-515299" data-slide="prev"><span class="carousel-control-prev-icon"></span> <span class="sr-only">Previous</span></a> <a class="carousel-control-next" href="#carousel-515299" data-slide="next"><span class="carousel-control-next-icon"></span> <span class="sr-only">Next</span></a>
			</div>
		</div>
	</div>
<!-- Fin del Carrusel -->

<div class="row">
     <!-- Blog Entries Column -->
     <div class="col-md-8">
	  <div>
	       <h1 class="my-4"><strong>Posts<small> más recientes</small></strong></h1>
	  </div>
     <!-- Blog Post -->
     <?php foreach($posts as $post): ?>
     <div class="card mb-4">
	  <?php foreach($imagenes as $imagen): ?>
	       <?php if($imagen['id_post']==$post['id']): ?>
	       <img class="card-img-top" src="uploads/imagenes/<?= $imagen['nombre_imagen']; ?>" alt="Card image cap">
	       <?php endif ?>
	  <?php endforeach; ?>
	  <div class="card-body">
	       <h2 class="card-title"><?= $post['titulo']; ?></h2>
	       <p class="card-text">
		    <?= $post['contenido']; ?>
	       </p>
	       <!--a href="#" class="btn btn-primary">Read More &rarr;</a-->
          </div>
          <div class="card-footer text-muted">
	       <p>
		    Autor: <?= $post['autor']; ?>
	       </p>
            <!--a href="#">Start Bootstrap</a-->
	  </div>
     </div>
     <?php endforeach; ?>

     </div>
     <!-- /.row -->
</div>
<!-- /.container -->
