<!DOCTYPE html>
<html lang="sp">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<title>Blog de CodeIgniter 4</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="container">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>  
  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
	  <a class="navbar-brand" href="/">Retornar Al Menú de Opciones</a>
    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      		<li class="nav-item active">
        		<a class="nav-link" href="/blog_Suelto">Blog</a>
	      	</li>
    	  	<li class="nav-item">
        		<a class="nav-link" href="/blog_Suelto/post">Post SencilloLink</a>
      		</li>
  		</ul>
	<!--       <li class="nav-item">
    	    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
	      </li>
    	</ul>
	    <form class="form-inline my-2 my-lg-0">
    	  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
	      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    	</form> -->
  </div>
</div>
</nav>
<div class="container"></div>
