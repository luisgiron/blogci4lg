<div class="container">
     <h1><?= (isset($title)? $title: 'Falta especificar el valor del Título'); ?></h1>
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <form method="post">
		    <div class="form-group">
			 <label for="post_title">Título</label>
			 <input id="" class="form-control" type="text" name="post_title" value="<?= $post['post_title']; ?>">
		    </div>
		    <div class="form-group">
			 <label for="post_title">Texto</label>
			 <textarea id="" class="form-control" name="post_content" rows="3"<?= $post['post_content']; ?>><?= $post['post_content']; ?></textarea>
		    </div>
		    <div class="form-group">
			 <button class="btn btn-success">Actualizar</button>
			 <a href="/blog_Suelto/" class="btn btn-secondary">Ver Todos los Títulos</a>
		    </div>
	       </form>
	  </div>
     </div>
</div>
