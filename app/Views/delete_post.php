<div class="container">
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <h1><?= (isset($title)? $title: 'Falta especificar el valor del Título'); ?></h1>
	  </div>
     </div>
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <form method="post" action="/blog_Suelto/new">
		    <div class="form-group">
			 <label for="post_title">Número del Registro:</label>
			 <input id="numero_registro" class="form-control" type="text" name="numero_registro" value="<?= $aide; ?>">
		    </div>
	       </form>
	  </div>
     </div>
     <div class="row">
	  <div class="col-12 col-md-8 offset-md-2">
	       <a href="/blog_Suelto/" class="btn btn-secondary">Ver Todos los Títulos</a>
	  </div>
     </div>
</div>
