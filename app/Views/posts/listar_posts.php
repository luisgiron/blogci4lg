<div class="container">
	<div class="row col-md-12">
		<h3>LISTAR POSTS</h3>		
	</div>

	<div class="alert alert-success" style="display: none;"></div>
	<div class="alert alert-danger" style="display: none;"></div>	

	<button id="btnAgregarPost" class="btn btn-success">Post Nuevo</button>
	<!-- <table class="table table-bordered table-responsive" style="margin-top: 20px"> -->
	<table class="table table-hover table-bordered" id="table_posts" style="margin-top: 20px">
		<thead>
			<tr>
				<td>Id</td>
				<td>Título</td>
				<td>Autor</td>
				<td>Fecha de Publicación</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody id="lista_de_posts">
		</tbody>
	</table>
</div>

<!--                     VENTANAS MODALES                       -->

<!-- Ventana Modal Para Registrar Posts -->
<div id="ventanaModalRegistrarPosts" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
	<!-- button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        </button -->
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <p>One fine body&hellip;</p> -->
        <form id="postsFormulario" action="" method="post" class="form-horizontal">
        	<div class="form-group">
        		<div class="col-md-10">
			       <input type="text" name="txtIde" id="txtIde" value="0" class="form-control">
        		</div>        		
        	</div>
        	<!-- input text -->      
        	<div class="form-group">
        		<label for="txtTituloPost" class="label-control col-md-5">Título Post :</label>
        		<div class="col-md-7">
        			<input type="text" name="txtTituloPost" id="txtTituloPost" class="form-control">
        		</div>        		
        	</div>
        	<!-- textarea -->
        	<div class="form-group">
        		<label for="txtContenido" class="label-control col-md-4">Contenido del Post :</label>
	        	<div class="col-md-8">
			     <textarea name="txtContenido" id="txtContenido" class="form-control"></textarea>
        		</div>
        	</div>
        	<div class="form-group">
        		<label for="txtAutor" class="label-control col-md-5">Autor del Post :</label>
        		<div class="col-md-7">
        			<input type="text" name="txtAutor" id="txtAutor" class="form-control">
        		</div>        		
        	</div>
	       <!--div class="form-group">
		   <label for="imagenPost" class="label-control col-md-5">Imagen del Post :</label>
		   <div class="col-md-5">
			   <input type="file" name="imagenPost" id="imagenPost">
		   </div>        		
	       </div-->
        	<!-- select -->
	       <!-- div class="form-group">
		    <label for="cmbEstados" class="label-control col-md-4">Estado:</label>
		    <div class="col-md-8">
			 <select name="cmbEstados" id="cmbEstados" class="form-control">
			 </select>
		    </div>
	       </div-->
	       <!--div class="form-group">
		    <label for="cmbMunicipios" class="label-control col-md-4">Municipio :</label>
		    <div class="col-md-8">
			 <select name="cmbMunicipios" id="cmbMunicipios" class="form-control"-->
			      <!--option value="0">Seleccione el Municipio ...</option-->
			 <!--/select>
		    </div>
	       </div-->
	       <!--div class="form-group">
		    <label for="cmbParroquias" class="label-control col-md-4">Parroquia :</label>
		    <div class="col-md-8">
			 <select name="cmbParroquias" id="cmbParroquias" class="form-control"-->
			      <!--option value="0">Seleccione la Parroquia ...</option-->
			 <!--/select>
		    </div>
	       </div-->
           <!-- radio -->
           <!-- hr>
			<fieldset class="form-group">
    			<div class="row">
      			<legend class="col-form-label col-sm-2 pt-0">Sexo :</legend>
	      			<div class="col-sm-10">
				        <div class="form-check">
				          <input class="form-check-input" type="radio" name="sexo" id="sexom" value="M" checked="true">
				          <label class="form-check-label" for="sexom" checked="false">
				            &nbsp;&nbsp;&nbsp;&nbsp;Masculino
				          </label>
				        </div><br>
				        <div class="form-check">
				          <input class="form-check-input" type="radio" name="sexo" id="sexof" value="F">k				          <label class="form-check-label" for="sexof">
				            &nbsp;&nbsp;&nbsp;&nbsp;Femenino
				          </label>
				        </div-->
<!-- 				        <div class="form-check disabled">
				          <input class="form-check-input" type="radio" name="sexo" id="sexo3" value="nosabe" disabled>
				          <label class="form-check-label" for="sexo3">
				            &nbsp;&nbsp;&nbsp;&nbsp;No sabe 
				          </label>
				        </div> -->
			      <!--/div>
				</div>
			</fieldset-->
			<!-- checkbox -->
			<!--hr>			
			  <div class="form-group row">
			    <div class="col-sm-3">Nacionalidad :</div>
			    <div class="col-sm-9">
			      <div class="form-check">
			        <input class="form-check-input" type="checkbox" name="extranjero" id="extranjero">
			        <label class="form-check-label" for="extranjero">
			          &nbsp;&nbsp;&nbsp;&nbsp;Extranjero
			        </label>
			      </div>			      
			    </div>
			  </div-->

			<!-- Botón para pruebas con jQuery -->
			<!--br>
			  <div class="form-group row">
			    <div class="col-sm-12">
			      <button type="button" class="btn btn-primary" id="btnVerificador">Verifique</button>
			    </div>
			  </div-->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnGuardarPost" class="btn btn-primary class_btn_guardar_post">Guardar Post</button>
        <button type="button" id="btnMostrarImagenAsociada" class="btn btn-success class_btn_mostrar_imagen_asociada" data-dismiss="modal">Mostrar Imagen Asociada</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Fin Ventana Modal Para Registrar Posts -->

<!-- Ventana Modal Para Incorporar imagen al Post -->
<div id="ventanaModalImagenPost" class="modal fade" tabindex="-1" role="dialog">
     <div class="modal-dialog modal-lg" role="document">
	  <div class="modal-content">
	       <div class="modal-header">
		    <h4 class="modal-title"></h4>
	       </div>
	       <div class="modal-body">
		    <form id="postsImagenFormulario" action="" method="post" class="form-horizontal">
			 <div class="form-group">
			     <div class="col-md-10">
				    <input type="text" name="txtIde" id="txtIde" value="0" class="form-control">
			     </div>        		
			 </div>
			 <div class="form-group">
			     <label for="imagenPost" class="label-control col-md-5">Imagen del Post :</label>
			     <div class="col-md-5">
				     <input type="file" name="imagenPost" id="imagenPost">
			     </div>        		
			 </div>
		    </form>
	       </div>
	       <div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" id="btnAsociarImagen" class="btn btn-primary">Asociar Imagen</button>
	       </div>
	  </div>
     </div>
</div>
<!-- Fin Ventana Modal Para Incorporar imagen al Post -->

<!-- Ventana Modal Para Eliminar Posts -->
<div id="ventanaModalEliminar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
	<!--button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button-->
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <p>One fine body&hellip;</p> -->
        ¿Desea Ud. Eliminar este Registro?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnEliminar" class="btn btn-primary">Eliminar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->
<!--Fin Ventana Modal Para Eliminar Posts -->
<!--Fin Ventanas Modales -->

<?php
     //Donde esté, para que el js que invoque desde footer_full, seteo esta variable base_url
?>
<script type="text/javascript">
	var base_url ="<?= base_url(); ?>";
</script>
