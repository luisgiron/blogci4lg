<div class="container">
	<div class="row col-md-12">
		<h3>MENSAJES - POSTS</h3>		
	</div>

     <div class="row">
	  <div class="col-sm-4">
	       <div class="alert alert-success" style="display: block;"><?= $mensaje; ?></div>
	       <!--div class="alert alert-danger" style="display: none;"></div-->	
	  </div>
	  <div class="col-sm-4">
	       <a href="<?= base_url(); ?>/entradas" class="btn btn-primary" role="button">Ver Lista de Posts</a>
	       <!--div class="alert alert-danger" style="display: none;"></div-->	
	  </div>
     </div>
</div>
