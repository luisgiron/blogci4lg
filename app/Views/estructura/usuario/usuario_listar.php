<?php
$btnEditar=
[
'name'=>'btneditar',
'id'=>'btneditar',
'type'=>'submit',
// 'content'=>'Editar',
'content'=>'<i class="fa fa-pencil-square-o"></i>',
'class'=>'btn btn-info'
];
$jsEliminar='onClick=alert("Eliminar...")';
$btnEliminar=
[
  'name'=>'btneliminar',
  'id'=>'btneliminar',
  'type'=>'submit',
  // 'content'=>'Eliminar',
  'content'=>'<i class="fa fa-trash"></i>',
  'class'=>'btn btn-danger'
];
$jsEditar='onClick=alert("Editar...")';
?>
  <!-- Content Wrapper. Contains page content -->
  <!-- div class="content-wrapper" -->
  <div class="">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
<!--         <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div> -->
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Usuarios</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="table_usuario" class="table table-bordered table-hover">
                  <thead>
                  <tr>
            		    <th scope="col">Id</th>
            		    <th scope="col">Nombre</th>
            		    <th scope="col">Correo</th>
            		    <th scope="col">Eliminado</th>
            		    <th scope="col">Acciones</th>          
                  </tr>
                  </thead>
                  <tbody>
		       <?php foreach ($usuarios as $usuario): ?>
		       <tr>
			 <th scope='row'><?=$usuario['id']; ?></th>
			 <td><?= $usuario['nombre']; ?></td>
			 <td><?= $usuario['correo']; ?></td>
			 <td><?= $usuario['eliminado']; ?></td>
			 <td>
			   <!--?= form_button($btnEditar,'',$jsEditar); ?-->
			   <a href="<?= base_url(); ?>/usuario/editar/<?= $usuario['id']; ?>" class="btn btn-warning" onClick="return confirm('¿Desea Editar Este Registro?');">Editar</a>
			   <!--?= form_button($btnEliminar,'',$jsEliminar)?-->
			   <a href="<?= base_url(); ?>/usuario/eliminar/<?= $usuario['id']; ?>" class="btn btn-danger" onClick="return confirm('¿Desea Eliminar Este Registro?')">Eliminar</a>
			 </td>
		       </tr>
		       <?php endforeach ?>
                  </tbody>
                  <tfoot>
                  <tr>
		    <th scope="col">Id</th>
		    <th scope="col">Nombre</th>
		    <th scope="col">Correo</th>
		    <th scope="col">Eliminado</th>
		    <th scope="col">Acciones</th>          
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
