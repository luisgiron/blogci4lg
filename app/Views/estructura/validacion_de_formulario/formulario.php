<div class="container">
   <h1>Validación de Formularios ... !</h1>
   	<?php if(isset($validacion)): ?>
   		<div class="text-danger">
   			<?= $validacion->listErrors(); ?>
   		</div>
   		<!--?php else: ?-->
   		<!--div class="text-danger">
   			Bbbbb ...
   		</div-->   			
	<?php endif; ?>
   <form method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <!-- <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"> -->
    <!-- El set_value('nombre_campo') es para mantener el valor escrito antes de la validación -->
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="correo" value="<?= set_value('correo'); ?>">    
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Clave</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="clave">
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Categoría</label>
    <select name="categoria" id="categoria" class="form-control">
    	<!-- <option value=""></option> -->
    	<?php foreach ($categorias as $categoria): ?>
    		<option <?= set_select('categoria',$categoria, TRUE); ?>  value="<?= $categoria; ?>"><?= $categoria; ?></option>    		
		<?php endforeach ?>    		
    </select>
    <!-- <input type="password" class="form-control" id="exampleInputPassword1"> -->
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Dia</label>
    <input type="date" class="form-control" id="exampleInputPassword1" name="fecha" id="fecha" value="<?= set_value('fecha') ;?>">
  </div>  
<?php
echo '<pre>';
print_r($_POST);
echo '</pre>';
?>

<!--    <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
 -->  <button type="submit" class="btn btn-primary">Submit</button>
</form>	
</div>