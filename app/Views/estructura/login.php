<!DOCTYPE html>
<!--html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Code Igniter 4 Pruebas | Logueo</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url(); ?>/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?= base_url(); ?>/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url(); ?>/assets/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head-->
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <!--a href="../../index2.html"><b>Admin</b>LTE</a-->
    <a href="#"><b>Ingreso de Usuario</b> Administración Del Blog</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Identifíquese para Comenzar su sesión</p>

      <form action="<?= base_url(); ?>/adminblog" id="miFormulario" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="usuario" name="login" id="idlogin">
          <div class="input-group-append">
            <div class="input-group-text">
              <!--span class="fas fa-envelope"></span-->
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Clave" name="clave" id="idclave">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-5">
            <!-- <button type="submit" class="btn btn-primary btn-block">Sign In</button> -->
            <button type="button" name="btnAutenticar" id="idBtnAutenticar" class="btn btn-primary btn-block">Autenticar</button>
          </div>
        <div class="input-group mb-3">
	       <div class="alert alert-warning" style="display: none;"></div>
        </div>
          <!-- /.col -->
        </div>
      </form>

<!--       <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> -->
      <!-- /.social-auth-links -->

      <p class="mb-1">
        <a href="forgot-password.html">Se me olvidó que te olvidé</a>
      </p>
      <p class="mb-0">
        <!--a href="register.html" class="text-center">Registrar un Nuevo Usuario</a-->
        <a href="/registrouser" class="text-center">Registrar un Nuevo Usuario</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?= base_url(); ?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url(); ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>/assets/dist/js/adminlte.min.js"></script>

</body>
</html>

<script type="text/javascript">
     var loginok=0;
     var claveok=0;
     $('#idBtnAutenticar').click(function()
     {
	  var formulariook=0;
	  ////alert(formulariook);
	  formulariook=verificarformulario();
	  ////alert('loginok'+loginok);
	  ////alert('claveok'+claveok);
	  ////alert(formulariook);
	  if(loginok==0)
	  {
	       $('.alert-warning').html('Login está en blanco').fadeIn().delay(3000).fadeOut('slow');          
	       $('#idlogin').focus();
	  }
	  if(claveok==0)
	  {
	       $('.alert-warning').html('La Clave está en blanco').fadeIn().delay(3000).fadeOut('slow');          
	       $('#idclave').focus();
	  }
	  if(loginok==1 && claveok==1)
	  {
	       //alert('son feos los doxs');
	       buscarUsuario();
	  }
     });
     function verificarformulario()
     {
	  alert('Verificando formulario');
	  loginok=verificarlogin();
	  claveok=verificarclave();
	  if(loginok==0)
	  {
	       return 0;
	  }
	  else if(claveok==0)
	  {
		    return 0;
	  }
	  else
	  {
	       //$('.alert-warning').html('Todo Ok').fadeIn().delay(3000).fadeOut('slow');          
	       return 1;
	  }
     }
     function verificarlogin()
     {
	  alert('Verificando login');
	  var login=$('#idlogin').val();
	  //alert(login);
	  if(login=='')
	  {
	       return 0;
	  }
	  else
	  {
	       return 1;
	  }
     }
     function verificarclave()
     {
	  alert('Verificando clave');
	  var clave=$('#idclave').val();
	  //alert(clave);
	  if(clave=='')
	  {
	       return 0;
	  }
	  else
	  {
	       return 1;
	  }
     }
     function buscarUsuario()
     {
	  alert('Estoy en la función buscar usuario');
	  var base="<?= base_url(); ?>/usuario/buscarlogin";
	  //var login = $('#idlogin').val();  
	  var data=
	  {
	       "login":$('#idlogin').val(),  
	       "clave":$('#idclave').val(),  
	  }
	  //alert(data.login);die();
	  $.ajax(
	  {
	       url:base,
	       type:"ajax",
	       method:'post',
	       data:data,
	       //data:{login:login},
	       dataType:'json',
	       beforeSend:function()
	       {
		    alert(' ... procesando la  petición, por favor espere un momento ...');
	       },
	       success:function(data)
	       {
		    alert('Paso?'+data.paso);        
		    alert('Error?'+data.error);        
		    //if(data===1)
		    if(data.paso===1)
			 alert('... Dejalo pasar ...');
		      //$('#miFormulario').submit();
		    else
		      //alert('Error de autenticación del usuario');
		      $('.alert-warning').html(' ... Error de autenticación del usuario ...!!!'+data.error).fadeIn().delay(3000).fadeOut('slow');          
		      $('#idlogin').val('');
		      $('#idclave').val('');
	       }
	   }
	  );
     } 
</script>
</html>
