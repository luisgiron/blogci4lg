<div class="container">
     <h1>Manipulación Múltiple de Imágenes</h1>

     <form method="post" enctype="multipart/form-data">

     <div class="form-group">
	  <label for="exampleFormControlFile1">Foto (.jpg, .jpeg, .png):</label>
	  <input type="file" name="miImagen" class="form-control-file" id="exampleFormControlFile1">
     </div>

     <button type="submit" class="btn btn-primary">Submit</button>

     </form>

<?php if(isset($imagen)): ?>
     <div class="row">
          <div class="col-md-4 mt-4">
               <h4>Original</h4>
               <img src="<?= $original; ?>" alt="Italian Trulix">
          </div>
          <?php foreach ($carpetas as $carpeta): ?>
               <div class="col-md-4-">
                    <h4>Modificado:</h4>
                    <img src="<?= $carpeta; ?>" alt="Italian Trulix">
               </div>
          <?php endforeach; ?>
     </div>
     Está seteada la imagen
<?php else: ?>
     NO Está seteada la imagen
<?php endif?>

</div>
