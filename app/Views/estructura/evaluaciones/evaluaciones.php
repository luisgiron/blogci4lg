<!--h2>Estoy en el Modulo de evaluaciones</h2-->
<div class="row">
     <div class="col-sm-10">
	  <div class="box box-primary">
	       <table class="table table-hover table-bordered" id="tabla_de_evaluaciones" style="margin-top: 20px">
		     <thead>
			     <tr>
				<td style="width:2%">Id</td>
				<td style="width:5%">Empleado</td>
				<td style="width:5%">1a Evaluación</td>
				<td style="width:5%">2a Evaluación</td>
				<td style="width:5%">3a Evaluación</td>
				<td style="width:5%">4a Evaluación</td>
				<td style="width:5%">Evaluación Def.</td>
			     </tr>
		     </thead>
		<tbody id="body_de_evaluaciones">
		</tbody>
	       </table>
	  </div>
     </div>
     <div class="col-sm-2">
	  <button type="button" class="btn btn-block btn-primary">Grabar</button>
     </div>
</div>

<script type="text/javascript">
     var base_url="<?= base_url(); ?>"
</script>
