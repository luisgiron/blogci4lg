  <footer class="">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery -->
<script src="<?= base_url(); ?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url(); ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url(); ?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url(); ?>/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url(); ?>/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>/assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>/assets/dist/js/demo.js"></script>
<!-- ¿Popper? -->
<!--script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script-->

<!-- Verifico el modulo en el que estoy para invocar el .js respectivo -->
</body>
</html>
<?php
    $request = \Config\Services::request();

    //$segmentos=$request->uri->getSegments();
    //var_dump($segmentos);

    $donde_estoy=$request->uri->getSegment(2);
    //var_dump($donde_estoy); //Se ve al ver el código fuente de la página
?>

<?php if($donde_estoy=='entradas') {?>
  <script src="<?= base_url(); ?>/my_js/posts.js"></script>
<?php } ?>
<?php if($donde_estoy=='usuario') {?>
  <script src="<?= base_url(); ?>/my_js/usuarios.js"></script>
<?php } ?>
<?php if($donde_estoy=='evaluaciones') {?>
  <script src="<?= base_url(); ?>/my_js/evaluaciones.js"></script>
<?php } ?>
