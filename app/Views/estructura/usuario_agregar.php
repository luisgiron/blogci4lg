<?php
//Este era formulario.php
if(isset($usuario))
{
	$id=$usuario['id'];
	$nombre=$usuario['nombre'];
	$correo=$usuario['correo'];
}
else
{
	$id='';
	$nombre='';
	$correo='';	
}
$inputNombre=
[
	'name'=>'nombre',
	'id'=>'nombre',
	'size'=>'50',
	'maxlength'=>'100',
	//'placeholder'=>'Sólo caracteres alfanuméricos y espacios',
	'value'=>$nombre
];
$inputCorreo=
[
	'name'=>'correo',
	'id'=>'correo',
	'size'=>'50',
	'maxlength'=>'100',
	//'placeholder'=>'algo@dominio.algo',
	'value'=>$correo
];
$inputId=
[
	'name'=>'id',
	'id'=>'id',
	'size'=>'50',
	'maxlength'=>'100',
	//'placeholder'=>'algo@dominio.algo',
	'value'=>$id,
	'type'=>'hidden'
];
$inputGuardar=
[
	'name'=>'misubmit',
	'id'=>'misubmit',
	'type'=>'submit',
	'class'=>"btn btn-info",
	'value'=>'Guardar'
];
$jsGuardar="onClick=return confirm('Desea Guardar los Cambios');";
$rutaRetorno=base_url() . '/usuario';

echo form_open('/usuario/guardar');

//echo form_label('Aide','id');
echo form_input($inputId);
echo "<br>";

echo form_label('Nombre','nombre');
echo form_input($inputNombre);
echo "<br>";
echo form_label('Correo','correo');
echo form_input($inputCorreo);
echo "<br>";
echo "<br>";
echo form_input($inputGuardar,'',"onClick=return confirm('Desea Guardar los Cambios');");

echo "<a href=$rutaRetorno class='btn btn-warning'>Cancelar</a>";
echo form_close();
?>