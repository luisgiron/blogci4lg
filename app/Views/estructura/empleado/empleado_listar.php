<div class="container">
	<div class="row col-md-12">
		<h3>CRUD jQuery-Ajax y DataTable - Empleados</h3>		
	</div>

	<div class="alert alert-success" style="display: none;"></div>
	<div class="alert alert-danger" style="display: none;"></div>	

	<button id="btnAgregarEmpleado" class="btn btn-success">Nuevo Registro</button>
	<!-- <table class="table table-bordered table-responsive" style="margin-top: 20px"> -->
	<table class="table table-hover table-bordered" id="table_empleado" style="margin-top: 20px">
		<thead>
			<tr>
				<td>Id</td>
				<td>Empleado</td>
				<td>Dirección</td>
				<td>Creado el</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody id="lista_de_empleados">
		</tbody>
	</table>
</div>

<!-- Ventana Modal Para Registrar Empleados -->
<div id="ventanaModalRegistrarEmpleado" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <p>One fine body&hellip;</p> -->
        <form id="empleadoFormulario" action="" method="post" class="form-horizontal">
        	<input type="text" name="txtIde" id="txtIde" value="0">
        	<!-- input text -->      
        	<div class="form-group">
        		<label for="txtNombreEmpleado" class="label-control col-md-5">Nombre Empleado :</label>
        		<div class="col-md-7">
        			<input type="text" name="txtNombreEmpleado" id="txtNombreEmpleado" class="form-control">
        		</div>        		
        	</div>
        	<!-- textarea -->
        	<div class="form-group">
        		<label for="txtDireccion" class="label-control col-md-4">Dirección :</label>
	        	<div class="col-md-8">
			     <textarea name="txtDireccion" id="txtDireccion" class="form-control"></textarea>
        		</div>
        	</div>
        	<!-- select -->
	       <div class="form-group">
		    <label for="cmbEstados" class="label-control col-md-4">Estado:</label>
		    <div class="col-md-8">
			 <select name="cmbEstados" id="cmbEstados" class="form-control">
			 </select>
		    </div>
	       </div>
	       <div class="form-group">
		    <label for="cmbMunicipios" class="label-control col-md-4">Municipio :</label>
		    <div class="col-md-8">
			 <select name="cmbMunicipios" id="cmbMunicipios" class="form-control">
			      <!--option value="0">Seleccione el Municipio ...</option-->
			 </select>
		    </div>
	       </div>
	       <div class="form-group">
		    <label for="cmbParroquias" class="label-control col-md-4">Parroquia :</label>
		    <div class="col-md-8">
			 <select name="cmbParroquias" id="cmbParroquias" class="form-control">
			      <!--option value="0">Seleccione la Parroquia ...</option-->
			 </select>
		    </div>
	       </div>
           <!-- radio -->
           <hr>
			<fieldset class="form-group">
    			<div class="row">
      			<legend class="col-form-label col-sm-2 pt-0">Sexo :</legend>
	      			<div class="col-sm-10">
				        <div class="form-check">
				          <input class="form-check-input" type="radio" name="sexo" id="sexom" value="M" checked="true">
				          <label class="form-check-label" for="sexom" checked="false">
				            &nbsp;&nbsp;&nbsp;&nbsp;Masculino
				          </label>
				        </div><br>
				        <div class="form-check">
				          <input class="form-check-input" type="radio" name="sexo" id="sexof" value="F">
				          <label class="form-check-label" for="sexof">
				            &nbsp;&nbsp;&nbsp;&nbsp;Femenino
				          </label>
				        </div>
<!-- 				        <div class="form-check disabled">
				          <input class="form-check-input" type="radio" name="sexo" id="sexo3" value="nosabe" disabled>
				          <label class="form-check-label" for="sexo3">
				            &nbsp;&nbsp;&nbsp;&nbsp;No sabe 
				          </label>
				        </div> -->
			      </div>
				</div>
			</fieldset>
			<!-- checkbox -->
			<hr>			
			  <div class="form-group row">
			    <div class="col-sm-3">Nacionalidad :</div>
			    <div class="col-sm-9">
			      <div class="form-check">
			        <input class="form-check-input" type="checkbox" name="extranjero" id="extranjero">
			        <label class="form-check-label" for="extranjero">
			          &nbsp;&nbsp;&nbsp;&nbsp;Extranjero
			        </label>
			      </div>			      
			    </div>
			  </div>

			<!-- Botón para pruebas con jQuery -->
			<br>
			  <div class="form-group row">
			    <div class="col-sm-12">
			      <button type="button" class="btn btn-primary" id="btnVerificador">Verifique</button>
			    </div>
			  </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnGuardarEmpleado" class="btn btn-primary">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Fin Ventana Modal Para Registrar Empleados -->


<!-- Ventana Modal Para Eliminar -->
<div id="ventanaModalEliminar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <p>One fine body&hellip;</p> -->
        ¿Desea Ud. Eliminar este Registro?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnEliminar" class="btn btn-primary">Eliminar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->
<!--Fin Ventana Modal Para Eliminar -->
<script type="text/javascript">
	var base_url ="<?= base_url(); ?>";
</script>
