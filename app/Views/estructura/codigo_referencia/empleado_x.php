<div class="container">
	<h3>Lista de Empleados con jQuery Ajax </h3>
	<button id="btnAgregarEmpleado" class="btn btn-success">Nuevo Registro</button>
	<table class="table table-bordered table-responsive" style="margin-top: 20px">
		<thead>
			<tr>
				<td>Id</td>
				<td>Empleado</td>
				<td>Dirección</td>
				<td>Creado el</td>
				<td>Acciones</td>				
			</tr>
		</thead>
		<tbody id="empleados">
		</tbody>
	</table>
</div>

<div id="ventanaModalEmpleado" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <!-- <p>One fine body&hellip;</p> -->
        <form id="empleadoFormulario" action="" method="post" class="form-horizontal">
        	<div class="form-group">
        		<label for="txtNombreEmpleado" class="label-control col-md-4">Nombre Empleado</label>
        		<div class="col-md-8">
        			<input type="text" name="txtNombreEmpleado" id="txtNombreEmpleado" class="form-control">
        		</div>        		
        	</div>
        	<div class="form-group">
        		<label for="txtDireccion" class="label-control col-md-4">Dirección</label>
	        	<div class="col-md-8">
    	    		<textarea class="form-control" name="txtDireccion"></textarea>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnGuardarEmpleado" class="btn btn-primary">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(function()
	{
		listarEmpleados();
		function listarEmpleados()
		{
			var base="<?= base_url(); ?>" + "/empleado/listar";
			//alert(base);
			$.ajax(
			{
				//url:"/empleado/listarempleados",
				url:base,
				type:"ajax",
				//async:false,
				dataType:'json',				
				beforeSend:function()
				{
					//alert(' ... procesando la  petición, por favor espere un momento ...');
				},
				success:function(data)
				{
					//alert('Que hiciste abusadora?');
					//console.log(data.length);
					//console.log(data);
					var html=''
					var i
					for (i=0; i<data.length; i++)
					{
						//alert(data[i].nombre);
						html +=
						'<tr>'+
							'<td>'+data[i].id+'</td>'+
							'<td>'+data[i].nombre+'</td>'+
							'<td>'+data[i].direccion+'</td>'+
							'<td>'+data[i].creado_en+'</td>'+
							'<td>'+
								'<a href="javascript:;" class="btn btn-info">Editar</a>'+
								'<a href="javascript:;" class="btn btn-danger">Eliminar</a>'+
							'</td>'+
						'</tr>'
						//console.log(html);
					}
					$('#empleados').html(html);
				},
				error:function()
				{
					alert('No le llego a la BD');
				}
			})
		}

		$('#btnAgregarEmpleado').click(function()
		{
			//alert('Loquitos ...');
			$('#ventanaModalEmpleado').modal('show');
			$('#ventanaModalEmpleado').find('.modal-title').text('Incorporación de Empleado ...');
			$('#empleadoFormulario').attr('action','<?= base_url();?>/empleado/agregar')
		});
		$("#btnGuardarEmpleado").click(function()
		{
			//alert('Guardar el Registro');
			var url = $('#empleadoFormulario').attr('action');
			//alert(url);
			var data = $('#empleadoFormulario').serialize();
			//alert(data);


			var empleado = $('input[name=txtNombreEmpleado]');
			var direccion = $('textarea[name=txtDireccion]');
			var resultado='';

			if(empleado.val()=='')
			{
				//alert('Vacío');
				empleado.parent().parent().addClass('has-error');
			}
			else
			{
				//alert('Con información');
				empleado.parent().parent().removeClass('has-error');
				resultado += '5';
			}
			if(direccion.val()=='')
			{
				//alert('Vacío');
				direccion.parent().parent().addClass('has-error');
			}
			else
			{
				//alert('Con información');
				direccion.parent().parent().removeClass('has-error');
				resultado += '4';
			}
			if(resultado=='54')
			{
				// alert(resultado);
				// alert('ok');
				$.ajax(
				{
					url:url,
					type:'ajax',
					dataType:'json',
					data:data,
					success:function(response)
					{
						console.log(response);
					},
					error:function()
					{
						alert('No funcionó');
					}
				});
			}
			else
			{
				alert(resultado);				
				alert('no ok');
			}
		});
				
	});
</script>
