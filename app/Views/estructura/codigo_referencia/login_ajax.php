<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div class="alert alert-warning" style="display: none;"></div>
<form method="post" id="miFormulario" action="<?= base_url(); ?>/usuario">
	<table>
		<tr>
			<td>
				<label>Login</label>
			</td>
			<td>
				<input type="text" name="login" id="idlogin">
			</td>
		</tr>
		<tr>
			<td>
				<label>Clave</label>
			</td>
			<td>
				<input type="password" name="clave" id="idclave">
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" name="btnAutenticar" id="idBtnAutenticar" value="Autenticar">
			</td>
		</tr>

	</table>
</form>
</body>


<script type="text/javascript">
	$('#idBtnAutenticar').click(function()
	{
		buscarUsuario();
	});
	function buscarUsuario()
	{
		//alert('Estoy en la función');
		var base="<?= base_url(); ?>/usuario/buscarlogin";
		var login = $('#idlogin').val();	
		$.ajax(
			{
				url:base,
				type:"ajax",
				method:'post',
				data:{login:login},
				dataType:'json',
				beforeSend:function()
				{
					alert(' ... procesando la  petición, por favor espere un momento ...');
				},
				success:function(data)
				{
					alert(data);				
					if(data===1)
						$('#miFormulario').submit();
					else
						//alert('Error de autenticación del usuario');
						$('.alert-warning').html(' ... Error de autenticación del usuario ...!!!').fadeIn().delay(3000).fadeOut('slow');					
						$('#idlogin').val('');
						$('#idclave').val('');
				}
			}
		);
	}	
</script>
</html>