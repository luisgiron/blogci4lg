<body>
<div class="container">
     <div class="page-header text-left">
	  <div class="alert alert-info">
	       <h1>Esta es la Página Principal de los Ejercicios de Code Igniter Versión 4</h1>
	  </div>
     </div>

     <!-- Primera Fila -->
     <div class="row">
	  <!-- Primera Columna -->
	  <div class="col-md-6">
	       <p align="center">Primera Columna</p>
	  </div>
	  <!-- Segunda Columna -->
	  <div class="col-mid-6">
	     <p align="center">Segunda Columna</p>
	  </div>
     </div>

     <!-- Segunda Fila -->
     <div class="row">
	  <!-- Primera Columna -->
	  <div class="col-md-6">
	      <p align="center">Este valor viene de un Helper ==></p>
	  </div>
	  <!-- Segunda Columna -->
	  <div class="col-md-6">
	      <p align="text-left">
		 <?= sumar(15, 3); ?>
	      </p>
	  </div>
     </div>

     <!-- Tercera Fila -->
     <div class="row">
	  <!-- Primera Columna -->
	  <div class="col-md-6">
	      <p align="center">Este valor viene de un Base de Datos ==></p>
	  </div>
	  <!-- Segunda Columna -->
	  <div class="col-md-6">
	      <p align="text-left">
		 <?= $nombre . ' ' . $correo; ?>
	      </p>
	  </div>
     </div>     

<div>

<script src="https://code.jquery.com/jquery-3.5.0.js" integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc=" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/index.js"></script>
</body>
</html>
