<br>
<div class="row">
     <div class="col-md-4">
	  <div class="alert alert-primary">
	       Esta es una alerta tipo Primary sin botón para cerrar
	  </div>
     </div>
</div>
<div class="row">
     <div class="col-md-4">
	  <div class="alert alert-info">
	       <button class="close" data-dismiss="alert">
		    &times;
	       </button>
	       Esta es una alerta tipo Info, Pero con un botón para cerrar
	  </div>
     </div>
</div>
