<div class="container">
<div>
     <h2>Recorro y Despliego los Valores de un Array con los registros de una tabla blogs</h2>
</div>
<div>
     <h1><?= (isset($title)? $title : 'Te Farrrta el Títuloxxss'); ?></h1>
</div>
<hr>
<!-- ?php foreach($valores as $valor): ?-->
<div class="row">
     <div class="col-12">
	  <a href="/blog_Suelto/new" class="btn btn-info">Agregar Otro Título</a>
     </div>
</div>
<div class="row">
     <div class="col-12">
	  <table class="table">
	       <thead class="thead-dark">
	       <tr>
		    <th>Id</th>
		    <th>Titulo</th>
		    <th>Descripcion</th>
		    <th>Acciones</th>
	       </tr>
	       </thead>
	       <tbody>
	       <?php foreach($posts as $post): ?>
		    <tr>
			 <td><?= $post['post_id']; ?></td>
			 <td><?= $post['post_title']; ?></td>
			 <td><?= $post['post_content']; ?></td>
			 <td>
			      <a href="/blog_Suelto/post/<?= $post['post_id']; ?>" class="btn btn-info">Ver Título</a>
			      <a href="/blog_Suelto/editar/<?= $post['post_id']; ?>" class="btn btn-warning">Editar Este Título</a>
			      <a href="/blog_Suelto/delete/<?= $post['post_id']; ?>" class="btn btn-danger">Eliminar Este Título</a>
			 </td>
		    </tr>
	       <?php endforeach; ?>
	  </table>
     </div>
</div>
</div>
