        			//Activacion del Data Table
					//1.- Ordenado de Forma Descendente según la primera columna
        			$('#table_empleado').DataTable(
        				{
        					"order":[[0,"desc"]],
        					"language":
        					{
								//"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
								"sProcessing":    "Procesando...",
						        "sLengthMenu":    "Mostrar _MENU_ registros",
						        "sZeroRecords":   "No se encontraron resultados",
						        "sEmptyTable":    "Ningún dato disponible en esta tabla",
						        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
						        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
						        "sInfoPostFix":   "",
						        "sSearch":        "Buscar:",
						        "sUrl":           "",
						        "sInfoThousands":  ",",
						        "sLoadingRecords": "Cargando...",
						        "oPaginate": {
						        	"sFirst":    "Primero",
						            "sLast":    "Último",
						            "sNext":    "Siguiente",
						            "sPrevious": "Anterior"
						        	},
        						"oAria": {
        							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
        							}
        					},
							"columnDefs": 
							[
							   {
							    "targets": [ 0 ],
							    "visible": false,
							    "searchable": false
							    }
							]
						}	
        			);
